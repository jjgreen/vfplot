/*
  main.c
  J.J. Green 2023
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "options.h"
#include "csv-grd.h"

#include <log.h>

#include <stdlib.h>
#include <stdio.h>


static int wrap(struct gengetopt_args_info*);

int main(int argc, char **argv)
{
  struct gengetopt_args_info info;

  if (options(argc, argv, &info) != 0)
    {
      fprintf(stderr, "failed to parse command line\n");
      return EXIT_FAILURE;
    }

  int err = wrap(&info);

  options_free(&info);

  return err;
}

static int get_options(struct gengetopt_args_info*, csv_grd_t*);

static int wrap(struct gengetopt_args_info *info)
{
  csv_grd_t opt;

  if (info->help_given)
    {
      options_print_help();
      return EXIT_SUCCESS;
    }

  if (info->version_given)
    {
      options_print_version();
      return EXIT_SUCCESS;
    }

  if (get_options(info, &opt) != 0)
    {
      fprintf(stderr, "error processing options\n");
      return EXIT_FAILURE;
    }

  if (opt.verbose)
    log_add_plain(stdout, LOG_INFO);
  else
    log_add_decorated(stderr, LOG_WARN);

  log_info("This is %s (version %s)", OPTIONS_PACKAGE, OPTIONS_VERSION);

  int err = csv_grd(&opt);

  if (err == 0)
    {
      log_info("done.");
      return EXIT_SUCCESS;
    }
  else
    {
      log_warn("failure");
      return EXIT_FAILURE;
    }
}

static int get_options(struct gengetopt_args_info *info, csv_grd_t *opt)
{
  opt->verbose = info->verbose_flag;

  if (! info->input_given)
    {
      fprintf(stderr, "The --input CSV is required\n");
      return 1;
    }

  opt->file.in = info->input_arg;

  if (info->grid_range_given)
    opt->grid.range.arg = info->grid_range_arg;
  else
    opt->grid.range.arg = NULL;

  if (info->grid_size_given)
    opt->grid.size.arg = info->grid_size_arg;
  else
    opt->grid.size.arg = NULL;

  if (info->pixel_flag)
    opt->grid.align = pixel_aligned;
  else
    opt->grid.align = node_aligned;

  /*
    Note, we use the --unnamed gengetopt option to specify the
    output files (since those are mandatory, we couldn't write
    two files to stdout), but gengetopt calls those "inputs"
    which is the usual use-case, that name can't be modified so
    looks a bit strange in the below
  */

  if (info->inputs_num != 2)
    {
      fprintf(stderr, "exactly two output CSV paths required\n");
      return 1;
    }

  opt->file.out[0] = info->inputs[0];
  opt->file.out[1] = info->inputs[1];

  return 0;
}

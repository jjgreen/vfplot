Bash completion scripts
-----------------------

Bash completion scripts which will be installed into the directory
specified by the `--with-bashcompletedir` option at configure.  These
use library functions from the [bash-completion][1] project, in
particular the `_parse_help` and `_filedir` functions.

The bash-completion package is widely available, on Debian install
the [bash-completion][2] package.

[1]: https://github.com/scop/bash-completion
[2]: https://packages.debian.org/stable/bash-completion

# vfplot completion -*- shell-script -*-

_vfplot()
{
    local cur prev words cword split
    _init_completion -s || return

    case "$prev" in
        --help | -h | --version | -V)
            return
            ;;
        --output | -o | --log-path | -L | --domain | -D | --animate-dir)
            _filedir
            return
            ;;
        --dump-vectors | --dump-domain | --graphic-state | -G | --histogram | --statistics)
            _filedir
            return
            ;;

        # these could/should? use the "list" target on our enumerated options
        # (probably another option) and would then be self-describing; but it
        # would add latency, and we don't have much change in these anticipated
        # so leave them as hard-coded for now

        --break)
            COMPREPLY=($(compgen -W "grid super midclean postclean none" -- "$cur"))
            return
            ;;
        --format | -F)
            COMPREPLY=($(compgen -W "auto csv mat gfs grd2" -- "$cur"))
            return
            ;;
        --log-level)
            COMPREPLY=($(compgen -W "trace debug info warn error fatal" -- "$cur"))
            return
            ;;
        --output-format | -O)
            COMPREPLY=($(compgen -W "eps povray" -- "$cur"))
            return
            ;;
        --placement | -p)
            COMPREPLY=($(compgen -W "hedgehog adaptive" -- "$cur"))
            return
            ;;
        --sort)
            COMPREPLY=($(compgen -W "none longest shortest bendiest straightest" -- "$cur"))
            return
            ;;
        --symbol | -S)
            COMPREPLY=($(compgen -W "arrow triangle" -- "$cur"))
            return
            ;;
        --test | -t)
            COMPREPLY=($(compgen -W "circular cylinder electro2 electro3 smoska" -- "$cur"))
            return
            ;;
    esac

    $split && return

    if [[ $cur == -* ]]; then
        COMPREPLY=($(compgen -W '$(_parse_help "$1")' -- "$cur"))
        [[ ${COMPREPLY-} == *= ]] && compopt -o nospace
        return
    fi

    _filedir
} &&
    complete -F _vfplot vfplot

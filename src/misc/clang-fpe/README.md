Clang floating point exception
------------------------------

In `src/libplot/plot.c` we have

``` c
double
  cmax = 2 * M_PI * CIRCULARITY_MAX,
  psi = a.length * a.curv;
if (psi > cmax)
  {
    psi = cmax;
    a.length = cmax / a.curv;
  }
```

which compiles without warning (with `-Wall`) on clang 9, 10, 11 and on
gcc 9, 10. On running the executable, all is fine with the gcc version,
but the clang version gives a FP (floating point) exception, division by
zero, at the if clause (according to gdb on the resulting core), and there
is a value of `a.curv` which is zero.

Note that `CIRCULARITY_MAX` is a positive constant, so `psi > cmax` implies
that `a.curv != 0`.  I find that adding this _redundant_ condition to the
conditional

``` c
if ((a.curv != 0) && (psi > cmax))
  {
    :
```

causes the exception goes away, and likewise if I have

``` c
if (psi > cmax)
  {
#ifdef __clang__
    assert(a.curv != 0);
#endif
    :
```

even when `NDEBUG` is defined.  So the `assert` has the side-effect of
fixing this incorrect compiler behaviour.

This is a bit strange; I asked a [question][1] on StackOverflow, but it
was closed, so I deleted it.  Before it was closed, [John Bollinger][2]
suggested looking at the compiler optimisation flags, I agree that would
be preferable.

I find that the `-ftrapping-math` flag fixes the exception in clang 10, 11,
but not in clang 9.  That the `-ffp-exception-behavior=maytrap` option fixes
the exception in clang 10, 11, but gives a compiler failure in clang 9

```
clang: error: unknown argument: '-ffp-exception-behavior=strict'
```

so it seems that the `assert` fix works for a wider range on clangs.
Clang 9 was released in 2019, so really not that old, but when it does
become deprecated (let's say "no longer available in Debian stable")
it would be worth using one of these flags (on just `plot.c`).

It may be worth creating a minimal example exhibiting this behaviour
and submitting as a bug report to the clang team. It's on my list
(and the reason this note is in a directory).

[1]: https://stackoverflow.com/questions/73967197/
[2]: https://stackoverflow.com/users/2402272/

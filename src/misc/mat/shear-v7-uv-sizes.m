# -*- octave -*-
# here the u, v matrices are different sizes, it should
# fail as vfplot input

xrange = [0 2];
yrange = [0 1];

n = 128;

u = repmat([1:n]./n, n, 1);
v = zeros(n + 1);

save -v7 shear-v7-uv-sizes.mat xrange yrange u v;

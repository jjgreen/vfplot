# -*- octave -*-
# xrange is not 1 x 2

xrange = [0 2 4];
yrange = [0 1];

n = 128;

u = repmat([1:n]./n, n, 1);
v = zeros(n);

save -v7 shear-v7-range-sizes.mat xrange yrange u v;

Animation
---------

Uses the `--animate` switch to generate EPS frames and some standard
tools (**convert** and **ffmpeg**) to convert those to an H.264 video
of the **vfplot** ellipse-packing dynamic.

The **vfplot** program should be compiled in `../../vfplot/`, then run
``` sh
make eps
make png
make mp4
```
The `png` target takes several minutes to run.

This needs the font Bitstream-Vera-Sans-Bold, on Debian-based systems
install the `ttf-bitstream-vera` package.

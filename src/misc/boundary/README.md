Boundary half-strokes
---------------------

Testbed for boundary clipping, the idea being to simulate
a half-stoke on the boundary, the stroke in the interior
of the boundary path is drawn, the exterior is not.  The
point being that as a consequence, the bounding box of the
boundary is not affected by the thickness of the line used
to draw the boundary, and this should makes alignment and so
composition simpler.

The mechanism is to use a combined stoke/clip, as suggested
in this [StackOverflow][1] answer by Ture Pålsson, to whom
many thanks.

[1]: https://stackoverflow.com/questions/76315374/

#ifndef PLOT_ERROR_H
#define PLOT_ERROR_H

#define ERROR_OK         0
#define ERROR_USER       1
#define ERROR_READ_OPEN  2
#define ERROR_WRITE_OPEN 3
#define ERROR_MALLOC     4
#define ERROR_BUG        5
#define ERROR_LIBGSL     6
#define ERROR_NODATA     7
#define ERROR_PTHREAD    8

#endif

#ifndef PLOT_DOMAIN_GEOJSON_H
#define PLOT_DOMAIN_GEOJSON_H

#include "plot/domain.h"

domain_t* domain_read(const char*);
int domain_write(const char*, const domain_t*);

#endif

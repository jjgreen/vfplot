#ifndef PLOT_ELLIPSE_DENSITY_H
#define PLOT_ELLIPSE_DENSITY_H

#include <plot/domain.h>
#include <plot/ellipse-base.h>

#include <stddef.h>

typedef bilinear_t ellipse_density_t;

ellipse_density_t* ellipse_density_new(const ellipse_base_t*);
void ellipse_density_destroy(ellipse_density_t*);
int ellipse_density_integrate(const ellipse_density_t*, const domain_t*, double*);

#endif

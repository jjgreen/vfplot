#ifndef PLOT_POLYGON_H
#define PLOT_POLYGON_H

#include <plot/bbox.h>

#include <geom2d/vec.h>

#include <stdio.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct
{
  size_t n;
  vec_t *v;
} polygon_t;

int polygon_init(size_t, polygon_t*);
void polygon_clear(polygon_t*);
int polygon_clone(const polygon_t*, polygon_t*);

int polygon_ngon(double, vec_t, size_t, polygon_t*);
int polygon_rect(bbox_t, polygon_t*);

int polygon_reverse(polygon_t*);

bool polygon_inside(const polygon_t*, vec_t);
bool polygon_contains(const polygon_t*, const polygon_t*);
int polygon_wind(const polygon_t*);
double polygon_area(const polygon_t*);
bbox_t polygon_bbox(const polygon_t*);

#endif

/*
  plot/scale.h
  Copyright (c) J.J. Green 2022
*/

#ifndef PLOT_SCALE_H
#define PLOT_SCALE_H

#define SCALE_BASE 60.0
#define SCALE_BODY SCALE_BASE
#define SCALE_CONTAIN (2.5 * SCALE_BASE)

#endif

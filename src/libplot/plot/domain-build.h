#ifndef PLOT_DOMAIN_BUILD_H
#define PLOT_DOMAIN_BUILD_H

#include <plot/domain.h>

#include <stddef.h>
#include <stdbool.h>

typedef size_t (db_nx_t)(const void*);
typedef size_t (db_ny_t)(const void*);
typedef bool (db_present_t)(const void*, size_t, size_t);
typedef void (db_xy_t)(const void*, size_t, size_t, double[2]);

domain_t* domain_build(db_nx_t*,
                       db_ny_t*,
                       db_present_t*,
                       db_xy_t*,
                       const void*);

#endif

#ifndef PLOT_DOMAIN_TYPE_H
#define PLOT_DOMAIN_TYPE_H

/*
  our domains are trees whose nodes contain a polygon a linked
  list of peers and a linked list of children. A child its peers
  are completely contained in their parent, and all peers are
  disjoint;
*/

typedef struct domain_node_t domain_node_t;

struct domain_node_t
{
  polygon_t p;
  domain_node_t *peer, *child;
};

struct domain_t
{
  domain_node_t *head;
};

#endif

/*
  scan-crop.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_SCAN_CROP_H
#define PLOT_SCAN_CROP_H

#include <plot/crop.h>

extern const crop_t scan_crop_default;

int scan_crop(const char*, crop_t*);

#endif

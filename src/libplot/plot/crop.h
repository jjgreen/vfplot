/*
  crop.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_CROP_H
#define PLOT_CROP_H

#include <stdbool.h>

typedef enum { crop_none, crop_simple } crop_type_t;

typedef struct
{
  crop_type_t type;
  struct {
    double min, max;
  } x, y;
} crop_t;

#endif

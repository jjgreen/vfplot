#ifndef PLOT_BILINEAR_H
#define PLOT_BILINEAR_H

#include <plot/bbox.h>
#include <plot/domain.h>

#include <stddef.h>
#include <stdio.h>
#include <stdbool.h>

typedef struct bilinear_t bilinear_t;
typedef int (bilinear_iterator_t)(bilinear_t*, size_t, size_t, void*);

bilinear_t* bilinear_new(size_t, size_t, size_t);
void bilinear_destroy(bilinear_t*);
void bilinear_bbox_set(bilinear_t*, bbox_t);
bbox_t bilinear_bbox_get(const bilinear_t*);
size_t bilinear_nx_get(const bilinear_t*);
size_t bilinear_ny_get(const bilinear_t*);
size_t bilinear_nz_get(const bilinear_t*);
void bilinear_xy_get(const bilinear_t*, size_t, size_t, double[2]);
void bilinear_z_get(const bilinear_t*, size_t, size_t, double*);
void bilinear_z_set(bilinear_t*, size_t, size_t, const double*);
bool bilinear_valid(const bilinear_t*, int, int);
int bilinear_csv_write(const bilinear_t*, FILE*);
int bilinear_eval(const bilinear_t*, double, double, double*);
int bilinear_each(bilinear_t*, bilinear_iterator_t*, void*);
int bilinear_integrate(const bilinear_t*, const domain_t*, double*);
bilinear_t* bilinear_curvature(const bilinear_t*);

domain_t* bilinear_node_domain(const bilinear_t*);
domain_t* bilinear_pixel_domain(const bilinear_t*);

#endif

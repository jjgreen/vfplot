/*
  svg-colours.h
  Copyright (c) J.J. Green 2023
*/

#ifndef SVG_COLOURS_H
#define SVG_COLOURS_H

struct svg_colour_t
{
  char *name;
  unsigned char r, g, b;
};

const struct svg_colour_t* svg_colour(const char *s);

#endif

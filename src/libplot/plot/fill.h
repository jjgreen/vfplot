#ifndef PLOT_FILL_H
#define PLOT_FILL_H

#include <plot/colour.h>

typedef enum { fill_none, fill_colour } fill_type_t;

typedef struct
{
  fill_type_t type;
  union
  {
    colour_t colour;
  };
} fill_t;

#endif

#ifndef PLOT_VFUN_H
#define PLOT_VFUN_H

typedef int (vfun_t)(void*, double, double, double*, double*);
typedef int (cfun_t)(void*, double, double, double*);
typedef void (sfun_t)(void*, double);

#endif

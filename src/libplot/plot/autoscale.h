#ifndef PLOT_AUTOSCALE_H
#define PLOT_AUTOSCALE_H

#include <plot/domain.h>
#include <plot/plot.h>

int autoscale(const domain_t*,
              vfun_t*, cfun_t*, sfun_t*, void*,
              const plot_opt_t*);

#endif

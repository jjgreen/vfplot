/*
  scan-colour.h
  Copyright (c) J.J. Green 2023
*/

#ifndef PLOT_SCAN_COLOUR_H
#define PLOT_SCAN_COLOUR_H

#include <plot/colour.h>

int scan_colour(const char*, colour_t*);

#endif

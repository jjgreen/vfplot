#ifndef PLOT_POTENTIAL_H
#define PLOT_POTENTIAL_H

double potential_V(double, double);
double potential_dV(double, double);

#endif

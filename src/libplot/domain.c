#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

#include <log.h>

#include "plot/units.h"
#include "plot/domain.h"
#include "plot/domain-type.h"

/* construct */

static domain_node_t* domain_node_new(void)
{
  domain_node_t *node;

  if ((node = malloc(sizeof(domain_node_t))) == NULL)
    return NULL;

  node->p.n = 0;
  node->p.v = NULL;
  node->peer = NULL;
  node->child = NULL;

  return node;
}

domain_t* domain_new(void)
{
  domain_t *dom;

  if ((dom = malloc(sizeof(domain_t))) == NULL)
    return NULL;

  dom->head = NULL;

  return dom;
}

/* destroy */

static void domain_node_destroy(domain_node_t *node)
{
  if (node != NULL)
    {
      polygon_clear(&(node->p));
      domain_node_destroy(node->child);
      domain_node_destroy(node->peer);
      free(node);
    }
}

void domain_destroy(domain_t *dom)
{
  domain_node_destroy(dom->head);
  free(dom);
}

/* iterate */

static int domain_node_each(domain_node_t *node, difun_t f, void *opt, int n)
{
  if (node == NULL)
    return 0;

  int err;

  if ((err = f(&(node->p), opt, n)) != 0)
    return err;

  if ((err = domain_node_each(node->child, f, opt, n + 1)) != 0)
    return err;

  return domain_node_each(node->peer, f, opt, n);
}

int domain_each(domain_t *dom, difun_t f, void *opt)
{
  return domain_node_each(dom->head, f, opt, 1);
}

/* clone */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int clone_fun(polygon_t *p, void *arg, int level)
{
  domain_t **new = arg;
  return domain_insert(*new, p);
}

#pragma GCC diagnostic pop

domain_t* domain_clone(const domain_t *dom)
{
  domain_t *new;

  if ((new = domain_new()) != NULL)
    {
      if (domain_each((domain_t*)dom, clone_fun, &new) == 0)
        {
          if (domain_orientate(new) == 0)
            return new;
        }
      domain_destroy(new);
    }
  return NULL;
}

/* shift and scale all points in domain */

typedef struct
{
  double x0, y0, M;
} scale_opt_t;

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int scale_fun(polygon_t *p, void *arg, int level)
{
  size_t n = p->n;
  vec_t *v = p->v;
  scale_opt_t *opt = arg;
  double
    M = opt->M,
    x0 = opt->x0,
    y0 = opt->y0;

  for (size_t i = 0 ; i < n ; i++)
    {
      v[i].x = M * (v[i].x - x0);
      v[i].y = M * (v[i].y - y0);
    }

  return 0;
}

#pragma GCC diagnostic pop

int domain_scale(domain_t *dom, double M, double x0, double y0)
{
  scale_opt_t opt = { .M = M, .x0 = x0, .y0 = y0 };
  return domain_each(dom, scale_fun, &opt);
}

/*
   enforce the orientation convention which is level n = 1, 2, 3, ..
   has orientation 1, -1, 1, ..
*/

#define ORIENT_REV(x) (x > 0 ? -1 : 1)
#define ORIENT_EQ(x, y) ((x)*(y) > 0)

static int domain_node_orientate_level(domain_node_t *node, int R)
{
  if (node == NULL)
    return 0;

  int w = polygon_wind(&(node->p));

  switch (w)
    {
    case 1: case -1: break;
    default:
      log_error("strange winding number %i", w);
      return 1;
    }

  if (!ORIENT_EQ(R, w))
    {
      if (polygon_reverse(&(node->p)) != 0)
	{
	  log_error("failed polygon reverse");
	  return 1;
	}
    }

  return
    domain_node_orientate_level(node->peer, R) ||
    domain_node_orientate_level(node->child, ORIENT_REV(R));
}

int domain_orientate(domain_t *dom)
{
  return domain_node_orientate_level(dom->head, 1);
}

/* check if a point is inside a domain */

static bool domain_node_inside(const domain_node_t *node, const vec_t v)
{
  if (node == NULL)
    return false;

  if (polygon_inside(&(node->p), v))
    return ! domain_node_inside(node->child, v);

  return domain_node_inside(node->peer, v);
}

bool domain_inside(const domain_t *dom, const vec_t v)
{
  return domain_node_inside(dom->head, v);
}

/*
  simple consistency check - the vertices of children should lie
  inside the parent (this is an incomplete check since polygons
  may be nonconvex). depth first search returns error on first
  violation found.
*/

static int domain_node_hcrec(const domain_node_t *node, polygon_t p)
{
  if (node == NULL)
    return 0;

  if (p.n == 0)
    {
      log_error("child node without a polygon");
      return 1;
    }

  polygon_t cp = node->p;

  for (size_t j = 0 ; j < cp.n ; j++)
    {
      if (polygon_inside(&p, cp.v[j]) == 0)
	{
	  log_error("vertex (%f, %f) is outside the polygon",
                    cp.v[j].x, cp.v[j].y);

	  for (size_t k = 0 ; k < p.n ; k++)
	    log_error(" (%f, %f)", p.v[k].x, p.v[k].y);

	  return 1;
	}
    }

  if (domain_node_hcrec(node->peer, p) != 0)
    return 1;

  if (domain_node_hcrec(node->child, cp) != 0)
    return 1;

  return 0;
}

static int domain_node_hierarchy_check(const domain_node_t *node)
{
  if (node == NULL)
    return 0;

  return domain_node_hcrec(node->child, node->p)
    + domain_node_hierarchy_check(node->peer);
}

int domain_hierarchy_check(const domain_t *dom)
{
  return domain_node_hierarchy_check(dom->head);
}

/* insert polygon */

domain_node_t* domain_node_insert(domain_node_t *node, const polygon_t *p)
{
  if (node == NULL)
    {
      if ((node = domain_node_new()) == NULL)
        return NULL;
      polygon_clone(p, &(node->p));
      return node;
    }

  for (domain_node_t *n = node ; n ; n = n->peer)
    {
      if (polygon_contains(p, &(n->p)))
	{
	  n->child = domain_node_insert(n->child, p);
	  return node;
	}
    }

  domain_node_t *head;

  if ((head = domain_node_new()) == NULL)
    return NULL;

  polygon_clone(p, &(head->p));
  head->peer = node;

  return head;
}

int domain_insert(domain_t *dom, const polygon_t *p)
{
  domain_node_t *head = domain_node_insert(dom->head, p);

  if (head == NULL)
    return 1;

  dom->head = head;

  return 0;
}

/* bounding box */

static int domain_node_bbox(const domain_node_t *node, bbox_t *bbox)
{
  bbox_t a = polygon_bbox(&(node->p));

  if (node->peer)
    {
      bbox_t b;
      if (domain_node_bbox(node->peer, &b) != 0)
        return 1;
      bbox_join(&a, &b, bbox);
    }
  else
    *bbox = a;

  return 0;
}

int domain_bbox(const domain_t *dom, bbox_t *bbox)
{
  return domain_node_bbox(dom->head, bbox);
}

static double domain_node_area(const domain_node_t *node)
{
  if (node == NULL) return 0;

  size_t n = node->p.n;

  if (n == 0) return 0;

  return
    polygon_area(&(node->p)) +
    domain_node_area(node->peer) -
    domain_node_area(node->child);
}

double domain_area(const domain_t *dom)
{
  return domain_node_area(dom->head);
}

static int domain_node_depth(const domain_node_t *node)
{
  int d = 0;

  if (node != NULL)
    {
      int
        d1 = domain_node_depth(node->peer),
        d2 = domain_node_depth(node->child) + 1;
      d = ((d1 > d2) ? d1 : d2);
    }

  return d;
}

int domain_depth(const domain_t *dom)
{
  return domain_node_depth(dom->head);
}

static int domain_node_width(const domain_node_t *node)
{
  int w = 0;

  if (node != NULL)
    w = domain_node_width(node->peer) + 1;

  return w;
}

int domain_width(const domain_t *dom)
{
  return domain_node_width(dom->head);
}

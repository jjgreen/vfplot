#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include <log.h>

#include "plot/hedgehog.h"
#include "plot/evaluate.h"
#include "plot/error.h"

int plot_hedgehog(const domain_t *dom,
                  vfun_t *fv, cfun_t *fc, void *field,
                  plot_opt_t *opt,
                  arrows_t *A)
{
  bbox_t bb = opt->bbox;
  double
    w = bbox_width(&bb),
    h = bbox_height(&bb),
    x0 = bb.x.min,
    y0 = bb.y.min,
    R = w / h;

  size_t
    N = opt->arrow.n,
    n = (int)floor(sqrt(N * R)),
    m = (int)floor(sqrt(N / R));

  if (n * m == 0)
    {
      log_error("empty %zix%zi grid - increase number of arrows", n, m);
      return ERROR_USER;
    }

  if ((A->v = malloc(n * m * sizeof(arrow_t))) == NULL)
    return ERROR_MALLOC;

  log_info("hedgehog grid is %zix%zi (%zi)", n, m, n * m);

  /* generate the field */

  evaluate_t evaluate = {
    .fv = fv,
    .fc = fc,
    .field = field,
    .aspect = opt->arrow.aspect
  };

  size_t k = 0;
  double dx = w / n, dy = h / m;

  for (size_t i = 0 ; i < n ; i++)
    {
      double x = x0 + (i + 0.5) * dx;

      for (size_t j = 0 ; j < m ; j++)
	{
	  double y = y0 + (j + 0.5) * dy;

	  vec_t v = {x, y};

	  if (! domain_inside(dom, v))
            continue;

	  arrow_t *Ak = A->v + k;

	  Ak->centre = v;

	  int err = complete_arrow(&evaluate, Ak);

	  switch (err)
            {
            case ERROR_OK : k++ ; break;
            case ERROR_NODATA: break;
            default: return err;
            }
	}
    }

  A->n = k;

  return ERROR_OK;
}

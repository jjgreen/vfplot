#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <log.h>

#include <plot/autoscale.h>
#include <plot/evaluate.h>
#include <plot/ellipse-base.h>
#include <plot/ellipse-density.h>

#include <math.h>

#define EGRID 64
#define EBRACKET 20
#define EBISECT 20
#define EDELTA 1.0

typedef struct
{
  const domain_t *dom;
  const arrow_opt_t *opt;
  const bbox_t bbox;
  evaluate_t *evaluate;
  sfun_t *scale;
} di_eval_t;

/*
  evaluate the ellipse density integral of a (scaled) field, this is
  done an a quite coarse grid (EGRID square).
*/

static int dI_eval(di_eval_t *arg, double *dI)
{
  int err = ERROR_OK;
  ellipse_base_t *base =
    ellipse_base_new(arg->bbox, EGRID, EGRID, arg->evaluate, arg->opt);

  if (base != NULL)
    {
      ellipse_density_t *density = ellipse_density_new(base);

      if (density != NULL)
        {
          err = ellipse_density_integrate(density, arg->dom, dI);
          ellipse_density_destroy(density);
        }
      else
        err = ERROR_MALLOC;

      ellipse_base_destroy(base);
    }
  else
    err = ERROR_MALLOC;

  return err;
}

/*
  evaluate the ellipse integral 0f the field when scaled by s,
  note that this mutates the field
*/

static int dI_scaled(di_eval_t *arg, double s, double *dI)
{
  arg->scale(arg->evaluate->field, s);
  int err = dI_eval(arg, dI);
  if (err != ERROR_OK)
    log_error("failed ellipse-integrate for scale %g", s);
  return err;
}

/*
  For a given arrow scaling, we can get an estimate of how many
  ellipses  should fit in the domain by integrating the ellipse
  density over the domain and multiplying that by the optimal
  circle packing constant.  Going in the other direction (from a
  number of ellipse, find the arrow scaling) is not trivial, since
  the relation is nonlinear due to the ellipse margins.  Nothing
  fancy here, we use bisection which is faily slow but pretty
  robust
*/

#define FMT_VALUE "%8.4f %8.2f"
#define FMT_HEAD "%8s %8s"
#define FMT_LINE "------------------"

int autoscale(const domain_t *dom,
              vfun_t *vector,
              cfun_t *curvature,
              sfun_t *scale,
              void *field,
              const plot_opt_t *opt)
{
  if (! opt->arrow.autoscale)
    {
      scale(field, opt->arrow.scale);
      return ERROR_OK;
    }

  log_info(FMT_LINE);
  log_info(FMT_HEAD, "scale", "num");
  log_info(FMT_LINE);

  const double EPACKOPT = M_PI / sqrt(12);

  evaluate_t evaluate =
    {
     .fv = vector,
     .fc = curvature,
     .field = field,
     .aspect = opt->arrow.aspect
    };

  arrow_opt_t arrow_opt =
    {
     .M = opt->place.adaptive.margin.rate,
     .bmaj = opt->place.adaptive.margin.major,
     .bmin = opt->place.adaptive.margin.minor,
     .scale = opt->page.scale
    };

  bbox_t bbox;

  if (domain_bbox(dom, &bbox) != 0)
    {
      log_error("failed to get bounding box");
      return ERROR_BUG;
    }

  di_eval_t di_arg =
    {
     .dom = dom,
     .opt = &arrow_opt,
     .bbox = bbox,
     .evaluate = &evaluate,
     .scale = scale
    };

  double
    s0 = 0,
    s1 = 1,
    n0, n1, dI;
  size_t n_target = opt->arrow.n;
  int err;

  /* evaluate n0 for scale s0 = 0 */

  if ((err = dI_scaled(&di_arg, s0, &dI)) != ERROR_OK)
    return err;

  n0 = EPACKOPT * dI;
  log_info(FMT_VALUE, s0, n0);

  if (n0 < n_target)
    {
      log_error("zero scaling gives %.0f, try", n0);
      log_error("- reducing --margin");
      log_error("- reducing --number (to below %.0f)", n0);
      log_error("- set --scale manually");
      return ERROR_USER;
    }

  /* evaluate n1 for scale s1 = 1 */

  if ((err = dI_scaled(&di_arg, s1, &dI)) != ERROR_OK)
    return err;

  n1 = EPACKOPT * dI;
  log_info(FMT_VALUE, s1, n1);

  /* double bracket [s0, s1] until n1 < n_target < n0 */

  for (size_t i = 0 ; (i < EBRACKET) && (n1 > n_target) ; i++)
    {
      s0 = s1;
      s1 *= 2;

      if ((err = dI_scaled(&di_arg, s1, &dI)) != ERROR_OK)
        return err;

      n0 = n1;
      n1 = EPACKOPT * dI;
      log_info(FMT_VALUE, s1, n1);
    }

  if (n1 > n_target)
    {
      log_error("failed to find autoscale bracket");
      return ERROR_USER;
    }

  /* bisect bracket until n0 - n1 < EDELTA */

  for (size_t i = 0 ; (i < EBISECT) && (n0 - n1 > EDELTA) ; i++)
    {
      double s = (s0 + s1) / 2;

      if ((err = dI_scaled(&di_arg, s, &dI)) != ERROR_OK)
        return err;

      double n = EPACKOPT * dI;
      log_info(FMT_VALUE, s, n);

      if (n > n_target)
        {
          s0 = s;
          n0 = n;
        }
      else
        {
          s1 = s;
          n1 = n;
        }
    }

  double s = (s0 + s1) / 2;
  scale(field, s);

  log_info(FMT_LINE);
  log_info("use --scale %.6f to avoid autoscale", s);

  return ERROR_OK;
}

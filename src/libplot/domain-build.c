#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/domain-build.h"

#include "plot/anynan.h"
#include "plot/gstack.h"
#include "plot/macros.h"
#include "plot/bbox.h"

#include <geom2d/vec.h>
#include <geom2d/mat.h>

#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>

/*
  returns a domain_t structure which is the piecewise linear boundary
  of the region on which the interpolant is defined.

  This used to be in bilinear.c, but in fact only needs a few properties
  of that, so we abstract those out and pass them here, now bilinear
  just needs wrapper functions for those properties -- this should make
  it easier to replace bilinear.c

  We use as a basic data structure a cell_t, a square with a datapoint
  at each corner

  TL   TR
    * *
    * *
  BL   BR

  encoded in an unsigned char
*/

typedef unsigned char cell_t;

#define CELL_BL ((cell_t) (1 << 0))
#define CELL_BR ((cell_t) (1 << 1))
#define CELL_TL ((cell_t) (1 << 2))
#define CELL_TR ((cell_t) (1 << 3))

#define CELL_NONE ((cell_t)0)
#define CELL_ALL  (CELL_BL | CELL_BR | CELL_TL | CELL_TR)


static cell_t** cell_array_new(size_t rows, size_t cols)
{
  if ((rows == 0) || (cols) == 0)
    return NULL;

  cell_t **cpp = calloc(rows, sizeof(cell_t*));

  if (cpp != NULL)
    {
      cell_t *cp = calloc(rows * cols, sizeof(cell_t));

      if (cp != NULL)
        {
          for (size_t i = 0 ; i < rows ; i++)
            cpp[i] = cp + i * cols;

          return cpp;
        }

      free(cpp);
    }

  return NULL;
}

static void cell_array_destroy(cell_t **cpp)
{
  free(*cpp);
  free(cpp);
}

static void point(size_t i, size_t j, cell_t m, gstack_t *st)
{
  switch (m)
    {
    case CELL_BL: break;
    case CELL_BR: i++ ; break;
    case CELL_TL: j++ ; break;
    case CELL_TR: i++ ; j++ ; break;
    }

  size_t v[2] = {i, j};

  gstack_push(st, (void*)v);
}

static int i2eq(const size_t *a, const size_t *b)
{
  return (a[0] == b[0]) && (a[1] == b[1]);
}

static void i2sub(const size_t *a, const size_t *b, size_t *c)
{
  for (size_t i = 0 ; i < 2 ; i++)
    c[i] = a[i] - b[i];
}

static bool i2colinear(const size_t *a, const size_t *b, const size_t *c)
{
  size_t u[2], v[2];

  i2sub(b, a, u);
  i2sub(c, b, v);

  return u[1] * v[0] == u[0] * v[1];
}

/* integer points & flag, used in trace() */

typedef struct
{
  size_t p[2];
  bool del;
} ipf_t;

/*
   given a list of n ipf_t, move all of the elements with a the del
   flag not set to the start of the list retaining the order, return
   the number of such
*/

static size_t compact(ipf_t *ipf, size_t n)
{
  size_t j = 0;

  for (size_t i = 0 ; i < n ; i++)
    {
      if (ipf[i].del)
        continue;
      ipf[j] = ipf[i];
      j++;
    }

  return j;
}

static size_t deldups(ipf_t *ipf, size_t n)
{
  for (size_t i = 0 ; i < n - 1 ; i++)
    if ( i2eq(ipf[i].p, ipf[i + 1].p) )
      ipf[i + 1].del = true;

  if ( i2eq(ipf[n - 1].p, ipf[0].p) )
    ipf[0].del = true;

  return compact(ipf, n);
}

/* delete colinear points in an list of ifps */

static size_t delcols(ipf_t *ipf, size_t n)
{
  if ( i2colinear(ipf[n-1].p, ipf[0].p, ipf[1].p) )
    ipf[0].del = true;

  for (size_t i = 0 ; i < n - 2 ; i++)
    if ( i2colinear(ipf[i].p, ipf[i + 1].p, ipf[i + 2].p) )
      ipf[i + 1].del = true;

  if ( i2colinear(ipf[n - 2].p, ipf[n - 1].p, ipf[0].p))
    ipf[n - 1].del = true;

  return compact(ipf, n);
}

static int trace_stack(cell_t **c, size_t i, size_t j, gstack_t *st)
{
  /*
    machine startup
    FIXME handle CELL_TL | CELL_BR here too
  */

  switch (c[i][j])
    {
    case CELL_TR:
    case CELL_TR | CELL_TL:
    case CELL_TR | CELL_TL | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TL:
    case CELL_TL | CELL_BL:
    case CELL_TL | CELL_BL | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_BL:
    case CELL_BL | CELL_BR:
    case CELL_BL | CELL_BR | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BR:
    case CELL_BR | CELL_TR:
    case CELL_BR | CELL_TR | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    default:
      return 1;
    }

  /*
    simple motion states
  */

 move_right:

  i++;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_TL | CELL_BR:
      c[i][j] = CELL_BR;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_TL | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TL | CELL_TR | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    default:
      return 1;
    }

 move_up:

  j++;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BL | CELL_TR:
      c[i][j] = CELL_TR;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BL | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    case CELL_BL | CELL_TL | CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    default:
      return 1;
    }

 move_left:

  i--;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_BR | CELL_TL:
      c[i][j] = CELL_TL;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_BR | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    case CELL_BR | CELL_BL | CELL_TL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TL, st);
      goto move_up;
    default:
      return 1;
    }

 move_down:

  j--;
  switch (c[i][j])
    {
    case CELL_NONE:
      goto move_end;
    case CELL_TR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TR | CELL_BL:
      c[i][j] = CELL_BL;
      point(i, j, CELL_TR, st);
      goto move_right;
    case CELL_TR | CELL_BR:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BR, st);
      goto move_down;
    case CELL_TR | CELL_BR | CELL_BL:
      c[i][j] = CELL_NONE;
      point(i, j, CELL_BL, st);
      goto move_left;
    default:
      return 1;
    }

 move_end:

  return 0;
}

static int ipf_load(gstack_t *st, size_t n, ipf_t *ipf)
{
  for (size_t i = 0 ; i < n ; i++)
    {
      if (gstack_pop(st, ipf[i].p) != 0)
        return 1;
      ipf[i].del = false;
    }

  return 0;
}

#define TRSTACK_INIT 512
#define TRSTACK_INCR 512

static int trace(db_xy_t *db_xy,
                 const void *grid,
                 cell_t **c,
                 size_t i, size_t j,
                 domain_t *dom)
{
  switch (c[i][j])
    {
    case CELL_ALL:
    case CELL_NONE:
      return 0;
    }

  gstack_t *st;

  if ((st = gstack_new(2 * sizeof(size_t), TRSTACK_INIT, TRSTACK_INCR)) == NULL)
    return 1;

  int err = 0;
  ipf_t *ipf = NULL;
  size_t n;

  if (trace_stack(c, i, j, st) != 0)
    err++;
  else
    {
      n = gstack_size(st);

      switch (n)
	{
	case 0:
	  err++;
	  break;

	case 1:
	case 2:
	  /* obviously degenerate, ignore and succeed  */
	  break;

	default:
	  if ((ipf = malloc(n * sizeof(ipf_t))) != NULL)
	    {
	      if (ipf_load(st, n, ipf) != 0)
		err++;
	    }

	}
    }

  gstack_destroy(st);

  if ((ipf == NULL) || err)
    return err;

  /*
    first look for consecutive points which are equal and delete the
    later one
  */

  if ((n = deldups(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /*
    mark colinear - note that if the ifp array contained consecutive
    equal points then both of these would be marked for deletion (in
    a-b-b-c, a-b-b and b-b-c are colinear so that both bs are deleted).
  */

  if ((n = delcols(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /*
    removing colinear nodes can introduce duplicates, so check for
    those again
  */

  if ((n = deldups(ipf, n)) < 3)
    {
      free(ipf);
      return 0;
    }

  /* transfer to polygon with m segments */

  polygon_t p;

  if (polygon_init(n, &p) != 0)
    {
      free(ipf);
      return 1;
    }

  for (size_t i = 0 ; i < n ; i++)
    {
      double xy[2];

      db_xy(grid, ipf[i].p[0] - 1, ipf[i].p[1] - 1, xy);

      p.v[i].x = xy[0];
      p.v[i].y = xy[1];
    }

  free(ipf);

  if ((domain_insert(dom, &p)) != 0)
    {
      return 1;
    }

  polygon_clear(&p);

  if (domain_orientate(dom) != 0)
    return 1;

  return 0;
}

domain_t* domain_build(db_nx_t *db_nx,
                       db_ny_t *db_ny,
                       db_present_t *db_present,
                       db_xy_t *db_xy,
                       const void *grid)
{
  size_t
    nx = db_nx(grid),
    ny = db_ny(grid);
  cell_t **cell;

  if ((cell = cell_array_new(nx + 1, ny + 1)) == NULL)
    return NULL;

  for (size_t i = 0 ; i < nx + 1 ; i++)
    for (size_t j = 0 ; j < ny + 1 ; j++)
      cell[i][j] = CELL_NONE;

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          if (db_present(grid, i, j))
            {
              cell[i + 1][j + 1] |= CELL_BL;
              cell[i + 1][j] |= CELL_TL;
              cell[i][j + 1] |= CELL_BR;
              cell[i][j] |= CELL_TR;
            }
        }
    }

  domain_t *dom;

  if ((dom = domain_new()) == NULL)
    goto cleanup;

  /* edge trace from each cell */

  for (size_t i = 0 ; i < nx + 1 ; i++)
    {
      for (size_t j = 0 ; j < ny + 1 ; j++)
	{
	  if (trace(db_xy, grid, cell, i, j, dom) != 0)
	    {
	      domain_destroy(dom);
	      dom = NULL;

	      goto cleanup;
	    }
	}
    }

 cleanup:

  cell_array_destroy(cell);

  return dom;
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/ellipse-base.h"

#include "plot/anynan.h"
#include "plot/arrow.h"
#include "plot/evaluate.h"
#include "plot/error.h"

#include <math.h>

ellipse_base_t* ellipse_base_new(bbox_t bbox, size_t nx, size_t ny,
                                 const evaluate_t *evaluate,
                                 const arrow_opt_t *opt)
{
  ellipse_base_t *base;

  if ((base = bilinear_new(nx, ny, 3)) == NULL)
    return NULL;

  bilinear_bbox_set(base, bbox);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          double xy[2];
          bilinear_xy_get(base, i, j, xy);

          arrow_t A = { .centre = { .x = xy[0], .y = xy[1] } };
          int err = complete_arrow(evaluate, &A);

          switch (err)
            {
              ellipse_t E;

            case ERROR_OK:

              arrow_ellipse(&A, opt, &E);

              double z[3] = { E.major, E.minor, E.theta };
              bilinear_z_set(base, i, j, z);
              break;

            case ERROR_NODATA:
              break;

            default:
              bilinear_destroy(base);
              return NULL;
            }
        }
    }

  return base;
}

void ellipse_base_destroy(ellipse_base_t *base)
{
  bilinear_destroy(base);
}

int ellipse_base_get(const ellipse_base_t *base, size_t i, size_t j, ellipse_t *E)
{
  double z[3];

  bilinear_z_get(base, i, j, z);

  if (anynan(3, z))
    return ERROR_NODATA;

  E->major = z[0];
  E->minor = z[1];
  E->theta = z[2];

  /* I don't think this is ever used, but it is sane (and cheap) */

  double xy[2];

  bilinear_xy_get(base, i, j, xy);

  E->centre.x = xy[0];
  E->centre.y = xy[1];

  return ERROR_OK;
}

size_t ellipse_base_nx(const ellipse_base_t *base)
{
  return bilinear_nx_get(base);
}

size_t ellipse_base_ny(const ellipse_base_t *base)
{
  return bilinear_ny_get(base);
}

bbox_t ellipse_base_bbox(const ellipse_base_t *base)
{
  return bilinear_bbox_get(base);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>

#include "plot/units.h"

typedef struct
{
  char c;
  char name[32];
  double ppt;
} unit_t;

#define PPT_PER_PT 0.99626401
#define PPT_PER_IN 72.0
#define PPT_PER_MM 2.83464567
#define PPT_PER_CM (10 * PPT_PER_MM)

static unit_t u[] =
  {
    {'P', "printer's point", PPT_PER_PT},
    {'p', "PostScript point", 1.0},
    {'i', "inch", PPT_PER_IN},
    {'m', "millimeter", PPT_PER_MM},
    {'c', "centimeter", PPT_PER_CM}
  };

static size_t num_units = sizeof(u) / sizeof(unit_t);

double unit_ppt(char c)
{
  for (size_t i = 0 ; i < num_units ; i++)
    if (u[i].c == c)
      return u[i].ppt;

  return -1.0;
}

const char* unit_name(char c)
{
  for (size_t i = 0 ; i < num_units ; i++)
    if (u[i].c == c)
      return u[i].name;
  return NULL;
}

int unit_list_stream(FILE *st)
{
  for (size_t i = 0 ; i < num_units ; i++)
    fprintf(st, "%c - %s\n", u[i].c, u[i].name);
  return 0;
}

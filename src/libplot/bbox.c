#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "plot/bbox.h"
#include "plot/macros.h"

void bbox_join(const bbox_t *a, const bbox_t *b, bbox_t *c)
{
  c->x.min = MIN(a->x.min, b->x.min);
  c->x.max = MAX(a->x.max, b->x.max);
  c->y.min = MIN(a->y.min, b->y.min);
  c->y.max = MAX(a->y.max, b->y.max);
}

double bbox_width(const bbox_t *b)
{
  return b->x.max - b->x.min;
}

double bbox_height(const bbox_t *b)
{
  return b->y.max - b->y.min;
}

double bbox_area(const bbox_t *b)
{
  return bbox_height(b) * bbox_width(b);
}

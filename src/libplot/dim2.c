#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#ifdef HAVE_PTHREAD_H
#include <pthread.h>
#ifdef WITH_PTHREAD_EXTRA
#include "plot/pthreadextra.h"
#endif
#endif

#include "plot/dim2.h"

#include "plot/error.h"
#include "plot/evaluate.h"
#include "plot/potential.h"
#include "plot/flag.h"
#include "plot/macros.h"
#include "plot/status.h"
#include "plot/gstack.h"
#include "plot/scale.h"

#include <geom2d/contact.h>
#include <log.h>

#include <rtree.h>

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

#ifndef INFINITY
#define INFINITY HUGE_VALF
#endif

/* optimal packing of circles */

#define EPACKOPT (M_PI / sqrt(12))

/*
  the schedule defines a series of parameters
  varied over the course of the dynamics run
  (essentially an annealing schedule) and applied
  over a subset of the particles.

  charge : the charge is multiplied by factor
  mass   : likewise
  rt     : potential truncation radius (from slj.h)
  rd     : deletion radius
  dmax   : maximum number deleted
*/

typedef struct
{
  double charge, mass, rd, rt;
  size_t dmax;
} schedule_t;

/* particle system */

#define PARTICLE_STALE FLAG(0)

typedef struct
{
  double charge, mass;
  double major, minor;
  mat_t M;
  vec_t v, dv, F;
  flag_t flag;
} particle_t;

/*
  we use pthreads for force accumulation and use
  these structures to pass arguments to the threads.

  tdata_t has the thread number and the offset and
  size of the edges array it processes. F and flag
  are private arrays of the forces and flags for the
  dim2 ellipses (so of size n2). tshared_t holds the
  shared data used by all threads.

  In the case that threads are not supported then
  we still used this structure, but all of the
  thread-specific parts of the structure are ifdef-ed
  out (and are, obviously, not used in this case)
*/

#ifdef HAVE_PTHREAD_H
#define PTHREAD_FORCES
#endif

typedef struct
{
  uint32_t *edge;
  particle_t *p;
  double rd, rt;
  size_t n2;

#ifdef PTHREAD_FORCES
  pthread_mutex_t mutex;
  pthread_barrier_t barrier[2];
  bool terminate;
#endif

} tshared_t;

typedef struct
{
  size_t id, off, size;
  vec_t *F;
  flag_t *flag;
  tshared_t *shared;
} tdata_t;

static int subdivide(size_t, size_t, size_t*, size_t*);
static void* forces(tdata_t*);

#ifdef PTHREAD_FORCES

static void* forces_worker(void*);
static int get_terminate_status(pthread_mutex_t*, bool*, bool*, int);
static int set_terminate_status(pthread_mutex_t*, bool*, bool);

#endif

/* temporary pw-distance struct */

typedef struct
{
  unsigned id;
  double d;
} pw_t;

/* expand p so it fits n2 particles (and put its new size in na) */

static int ensure_alloc(size_t n2, particle_t **pp, size_t *na)
{
  particle_t *p;

  if (n2 <= *na)
    return 0;

  if ((p = realloc(*pp, n2 * sizeof(particle_t))) == NULL)
    return 1;

  *pp = p;
  *na = n2;

  return 0;
}

/*
  signal handler

  the dim2 iteration can take a while, so we install
  a handler for SIGINT (control-c) which schedules
  a graceful exit at the end of the next cycle.

  this needs a file-scope value exitflag which is
  checked at the end of each outer iteration
*/

#ifdef HAVE_SIGNAL_H

static int exitflag = 0;

static void setexitflag(int sig)
{
  exitflag = 1;

#ifdef HAVE_STRSIGNAL
  log_error("[signal] caught %i (%s), halt scheduled",
            sig, strsignal(sig));
#else
  log_error("[signal] caught %i, halt scheduled", sig);
#endif
}

#endif

static int neighbours(particle_t*, size_t, uint32_t**, uint32_t*);
static nbr_t* nbrs_populate(size_t, uint32_t*, size_t, particle_t*);

/* compares particles by flag */

static int ptcomp(const void *va, const void *vb)
{
  const particle_t *a = va, *b = vb;
  return
    GET_FLAG(a->flag, PARTICLE_STALE) -
    GET_FLAG(b->flag, PARTICLE_STALE);
}

/* compare pw_ts by d */

static int pwcomp(const void *va, const void *vb)
{
  const pw_t *a = va, *b = vb;
  double da = a->d, db = b->d;

  if (da < db)
    return -1;
  else if (da > db)
    return 1;
  else
    return 0;
}

/*
  for t in [0, 1], defines a spline which is constant z0 in [0, t0],
  constant z1 in [t1, 1], and a sinusoid in between.
*/

static double sinspline(double t, double t0, double z0, double t1, double z1)
{
  if (t < t0) return z0;
  if (t > t1) return z1;

  return z0 + (z1 - z0) * (1.0 - cos(M_PI * (t - t0) / (t1 - t0))) / 2.0;
}

/*
  phases of the schedule
  - start, for charge buildup
  - clean, delete overlappers
  - detrunc, de-truncate the potential
*/

#define START_T0 0.0
#define START_T1 0.1

#define CLEAN_T0 0.3
#define CLEAN_T1 0.6

#define CLEAN_RADIUS 0.5
#define CLEAN_DELMAX 128

#define TRUNCATE_T0 0.6
#define TRUNCATE_T1 0.7

#define TRUNCATE_R0 0.90
#define TRUNCATE_R1 0.00

/* breakpoints defined in terms of these */

#define BREAK_SUPER (0.95 * CLEAN_T0)
#define BREAK_MIDCLEAN ((CLEAN_T1 + CLEAN_T0) / 2.0)
#define BREAK_POSTCLEAN (1.05 * CLEAN_T1)

static void schedule(double t, schedule_t *s)
{
  s->mass = 1.0;
  s->charge = sinspline(t, START_T0, 0.0, START_T1, 1.0);
  s->rd = sinspline(t, CLEAN_T0, 0.0, CLEAN_T1, CLEAN_RADIUS);
  s->rt = sinspline(t, TRUNCATE_T0, TRUNCATE_R0, TRUNCATE_T1, TRUNCATE_R1);
  s->dmax = sinspline(t, CLEAN_T0, 0.0,	CLEAN_T1, CLEAN_DELMAX);
}

/* perram-werthiem distance failures, always a bug */

static void pw_error_p(size_t k, particle_t p)
{
  log_error("  e%i (%g, %g), [%g, %g, %g]",
            (int)k,
            p.v.x, p.v.y,
            p.M.a, p.M.b, p.M.d);
}

static void pw_error(vec_t rAB, particle_t p1, particle_t p2)
{
  log_error("BUG: pw fails, rAB = (%g, %g)", rAB.x, rAB.y);
  pw_error_p(1, p1);
  pw_error_p(2, p2);
}

/*
  set mass & charge on a particle p, for a circle equal to the
  radius by mutiplied by a time dependent constant
*/

static void set_mq(particle_t *p, double mC, double qC)
{
  /*
    here one can play with various scalings, if these are circles
    then sqrt(major * minor)  above is right (ie, large and small
    exert the same pressure) but for ellipses r = major might be
    best (by anisotropy the pressure is felt along the major axis).
    Anything superlinear should favour the large against the small,
    a bit of this might be desirable. so mayby pow(p->major, 1.2)
    or something?
  */

  double r = p->major;

  p->mass = r * mC;
  p->charge = r * qC;
}

/* utility struct for kinetic energy drop -k option */

typedef struct
{
  unsigned iter;
  bool done;
  double kedB, drop;
} wait_t;

/* streams used in dim2 (which need to be closed) */

typedef struct
{
  FILE *histogram, *escaped;
} dim2_streams_t;

/* macros for the histogram */

#define HIST_BINWIDTH 0.025
#define HIST_BINS 80
#define HIST_DP 3

static int dim2_streams(dim2_opt_t *opt,
                        dim2_streams_t *st,
                        arrows_t *A,
                        nbrs_t *N)
{
  int err;
  vec_t zero = {0, 0};
  arrow_opt_t *arrow_opt = opt->arrow_opt;

  /* timestep */

  double dt = opt->plot.place.adaptive.timestep;

  /* initialise schedules */

  schedule_t sched;

  schedule(0.0, &sched);

  /*
    n2 number of dim 2 arrows, na number of arrows allocated
  */

  size_t n2 = 0, na = 0;

  /* domain dimensions */

  double
    w = bbox_width(&(opt->plot.bbox)),
    h = bbox_height(&(opt->plot.bbox)),
    x0 = opt->plot.bbox.x.min,
    y0 = opt->plot.bbox.y.min;

  /*
    area of bounding box of the domain (not the area of the
    domain, since we use this value to calculate the size of
    the initial grid of ellipses)
  */

  double dbbA;

  {
    bbox_t bbox;

    if (domain_bbox(opt->dom, &bbox) != 0)
      {
        log_error("failed domain bounding box");
        return ERROR_BUG;
      }

    dbbA = bbox_area(&bbox);
  }

  double dA;

  if ((dA = domain_area(opt->dom)) <= 0)
    {
      log_error("domain area is %f?", dA);
      return ERROR_BUG;
    }

  /*
    estimate number we can fit in, the density of the optimal
    circle packing is pi / sqrt(12), the mean density of ellipes
    opt->density
  */

  size_t
    no = dbbA * EPACKOPT * opt->density,
    ni = no * opt->plot.place.adaptive.overfill;

  status("estimate", no);

  if (ni < 1)
    {
      log_error("bad initial particles estimate %zi", ni);
      return ERROR_NODATA;
    }

  /* find the grid size */

  double R = w / h;
  size_t
    nx = floor(sqrt(ni * R)),
    ny = floor(sqrt(ni / R));

  if ((nx < 1) || (ny < 1))
    {
      log_error("initial dim2 grid is %zi x %zi, bad domain?", nx, ny);
      return ERROR_NODATA;
    }

  status("fill grid", nx * ny);

  /*
    allocate for ni > nx.ny, we will probably be adding more arrows
    later
  */

  particle_t *p = NULL;

  if (ensure_alloc(ni, &p, &na) != 0)
    return ERROR_MALLOC;

#ifdef PTHREAD_FORCES

  size_t nt = opt->plot.threads;

#else

  size_t nt = 1;

#endif

#ifdef HAVE_SIGNAL_H

  static struct sigaction act, oldact;

  act.sa_handler = setexitflag;
  act.sa_flags = 0;
  sigemptyset(&act.sa_mask);

  if (sigaction(SIGINT, &act, &oldact) == -1)
    log_error("failed to install signal handler");

#endif

  /* ratio of total ellipse area to domain area */

  double eprop = 0.0;

  /* filename extension */

  char ext[4];

  switch (opt->plot.file.output.format)
    {
    case output_format_eps:
      strcpy(ext, "eps");
      break;
    case output_format_povray:
      strcpy(ext, "inc");
      break;
    }

  /* generate an initial dim2 particle set on a regular grid */

  double dx = w / (nx + 2);
  double dy = h / (ny + 2);

  for (size_t i = 0 ; i < nx ; i++)
    {
      double x = x0 + (i + 1.5) * dx;

      for (size_t j = 0 ; j < ny ; j++)
        {
          double y = y0 + (j + 1.5) * dy;
          vec_t v = {x, y};

          if (! domain_inside(opt->dom, v))
            continue;

	  arrow_t A = { .centre = v };

          err = complete_arrow(opt->evaluate, &A);

          switch (err)
            {
	      ellipse_t E;

            case ERROR_OK :
	      arrow_ellipse(&A, arrow_opt, &E);
              ellipse_mat(&E, &(p[n2].M));
	      p[n2].v = E.centre;
	      p[n2].dv = zero;
	      p[n2].major = E.major;
	      p[n2].minor = E.minor;
	      p[n2].flag = 0;
	      n2++ ;
	      break;
            case ERROR_NODATA: break;
            default: return err;
            }
        }
    }

  status("initial", n2);

  if (n2 == 0)
    {
      log_error("nothing to plot");
      return ERROR_NODATA;
    }

  /* initial neighbour mesh */

  uint32_t
    nedge = 0,
    *edge = NULL;

  if ((err = neighbours(p, n2, &edge, &nedge)) != ERROR_OK)
    {
      log_error("failed to generate initial neighbour mesh");
      return err;
    }

  if (nedge < 2)
    {
      log_error("only %i edges", nedge);
      return ERROR_NODATA;
    }

  /* setup data shared between threads */

  tshared_t tshared;

#ifdef PTHREAD_FORCES

  tshared.terminate = false;

  /* mutex (for global flags) and barriers */

  if ((err = pthread_mutex_init(&(tshared.mutex), NULL)) != 0)
    {
      log_error("failed to init mutex: %s", strerror(err));
      return ERROR_PTHREAD;
    }

  pthread_barrierattr_t battr;

  if ((err = pthread_barrierattr_init(&battr)) != 0)
    {
      log_error("error at barrier attribute init: %s", strerror(err));
      return ERROR_PTHREAD;
    }

  if ((err = pthread_barrierattr_setpshared(&battr, PTHREAD_PROCESS_PRIVATE)) != 0)
    {
      log_error("error at barrier set shared: %s", strerror(err));
      return ERROR_PTHREAD;
    }
  else
    {
      for (size_t k = 0 ; k < 2 ; k++)
	{
	  if ((err = pthread_barrier_init(tshared.barrier + k, &battr, nt + 1)) != 0)
	    {
	      log_error("error at barrier %zi init: %s", k, strerror(err));
	      return ERROR_PTHREAD;
	    }
	}

      if ((err = pthread_barrierattr_destroy(&battr)) != 0)
	{
	  log_error("failed to destroy thread attribute: %s", strerror(err));
	  return ERROR_PTHREAD;
	}
    }

#endif

  /* thread data */

  tdata_t tdata[nt];

#ifdef PTHREAD_FORCES

  /* thread attribute */

  pthread_attr_t attr;

  if ((err = pthread_attr_init(&attr)) != 0)
    {
      log_error("failed to init thread attribute: %s", strerror(err));
      return ERROR_PTHREAD;
    }

  if ((err = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE)) != 0)
    {
      log_error("failed to set detach state: %s", strerror(err));
      return ERROR_PTHREAD;
    }

  pthread_t thread[nt];

  for (size_t k = 0 ; k < nt ; k++)
    {
      tdata[k].id = k;
      tdata[k].shared = &tshared;
      err = pthread_create(thread + k, &attr, forces_worker, tdata + k);
      if (err)
	{
	  log_error("failed to create thread %zi: %s", k, strerror(err));
	  return ERROR_PTHREAD;
	}
    }

  /* clean up attribute */

  if ((err = pthread_attr_destroy(&attr)) != 0)
    {
      log_error("failed to destroy thread attribute: %s", strerror(err));
      return ERROR_PTHREAD;
    }

#else

  tdata[0].shared = &tshared;

#endif

  /* grid break */

  if (opt->plot.place.adaptive.breakout == break_grid)
    {
      log_info("[break at grid generation]");
      goto output;
    }

  /* set the initial physics */

  for (size_t i = 0 ; i < n2 ; i++)
    set_mq(p + i, sched.mass, sched.charge);

  /* particle cycle */

  const char
    hline[] = "----------------------------------------------",
    head[]  = "  n   sym ocl fex  edge    e/g      ke  prop";

  log_info(hline);
  log_info(head);
  log_info(hline);

  iterations_t iter = opt->plot.place.adaptive.iter;
  wait_t wait;

  wait.drop = opt->plot.place.adaptive.kedrop;
  wait.iter = iter.main * TRUNCATE_T1;
  wait.kedB = 0.0;
  wait.done = ! (wait.drop > 0);

  for (size_t i = 0 ; (i < iter.main) || (!wait.done) ; i++)
    {
      if (st->histogram)
	{
	  unsigned int hist[HIST_BINS] = {0};

	  /* pw distance histogram */

	  for (size_t j = 0 ; j < (size_t)nedge ; j++)
	    {
              size_t
                id0 = edge[2 * j],
                id1 = edge[2 * j + 1];
	      vec_t rAB = vsub(p[id1].v, p[id0].v);
              double x;

              contact_mt(rAB, p[id0].M, p[id1].M, &x, NULL);

	      if (x < 0)
		{
		  pw_error(rAB, p[id0], p[id1]);
		  continue;
		}

	      double d = sqrt(x);
	      size_t bid = (int)round(d / HIST_BINWIDTH);

	      if (bid < HIST_BINS)
                hist[bid]++;
	    }

	  for (size_t j = 0 ; j < HIST_BINS ; j++)
	    fprintf(st->histogram, "%zi,%.*f,%u\n", i,
		    HIST_DP, j * HIST_BINWIDTH, hist[j]);
	}

      /*
        inner cycle which should be short -- a duration
        over which the neighbours network is valid.
      */

      double T = 0;

      for (size_t j = 0 ; j < iter.euler ; j++)
	{
	  T = (double)(i * iter.euler + j) / (iter.euler * iter.main);

	  schedule(T, &sched);

	  if (opt->plot.place.adaptive.animate)
	    {
	      int bufsz = 32 + strlen(opt->plot.place.adaptive.animate);
	      char buf[bufsz];
	      plot_opt_t plot = opt->plot;

	      snprintf(buf, bufsz,
                       "%s/anim.%.4zi.%.4zi.%s",
                       opt->plot.place.adaptive.animate,
                       i, j, ext);

	      plot.file.output.path = buf;

	      if (n2 > A->n)
		{
		  arrow_t *Av = realloc(A->v, n2 * sizeof(arrow_t));

		  if (!Av) return ERROR_MALLOC;

		  A->v = Av;
		}

	      A->n = n2;

	      for (size_t k = 0 ; k < n2 ; k++)
		{
                  /* coverity[deref_parm] */
                  arrow_t *arrow = &(A->v[k]);
		  arrow->centre = p[k].v;
		  complete_arrow(opt->evaluate, arrow);
		}

	      nbrs_t N2;

              N2.n = nedge;
              N2.v = nbrs_populate(nedge, edge, n2, p);

	      if (!N2.v) return ERROR_BUG;

              err = plot_frame(opt->dom, A, &N2, &plot);

	      free(N2.v);

	      if (err != ERROR_OK)
		{
		  log_error("failed animate write of %zi arrows to %s",
                            A->n, plot.file.output.path);
		  return err;
		}
	    }

	  /* reset forces */

	  for (size_t k = 0 ; k < n2 ; k++)
            p[k].F = zero;

	  /* accumulate forces */

          size_t eoff[nt];
	  size_t esz[nt];

	  if (subdivide(nt, nedge, eoff, esz) != 0)
	    {
	      log_info("failed %zi-partition of ellipse set", nt);
	      return ERROR_BUG;
	    }
	  else
	    {
	      /*
                each thread gets its own array of vectors to store the
                accumulated forces, so there is no need for a mutex
	      */

	      vec_t F[nt * n2];
	      flag_t flag[nt * n2];

	      for (size_t k = 0 ; k < nt * n2 ; k++)
		{
		  F[k] = zero;
		  flag[k] = 0;
		}

	      tshared.edge = edge;
	      tshared.p = p;
	      tshared.rd = sched.rd;
	      tshared.rt = sched.rt;
	      tshared.n2 = n2;

	      for (size_t k = 0 ; k < nt ; k++)
		{
		  tdata[k].off = eoff[k];
		  tdata[k].size = esz[k];
		  tdata[k].F = F + k * n2;
		  tdata[k].flag = flag + k * n2;
		}

#ifdef PTHREAD_FORCES

	      err = pthread_barrier_wait( &(tshared.barrier[0]) );
	      if ((err != 0) && (err != PTHREAD_BARRIER_SERIAL_THREAD))
		{
		  log_error("error on barrier 0 wait: %s", strerror(err));
		  return ERROR_PTHREAD;
		}

	      /* the threads calculate forces here */

	      err = pthread_barrier_wait( &(tshared.barrier[1]) );
	      if ((err != 0) && (err != PTHREAD_BARRIER_SERIAL_THREAD))
		{
		  log_error("error on barrier 1 wait: %s", strerror(err));
		  return ERROR_PTHREAD;
		}

#else
	      /*
		in the non-threaded version we just call the forces() function
                directly, rather than having each thread's forces_worker() do it
                -- we pass it the thread data struct, which, due to ifdefs,
                contains no information about threads (so this a bad name for it)
	      */

	      forces((void*)&tdata);
#endif
	      /*
		now dump the forces which are nt blocks of size n2

		   F = [F1, ... Fn2, F1, ... Fn2, ... ]

	        into the particle array
	      */

	      for (size_t k = 0 ; k < n2 ; k++)
		{
		  vec_t Fsum = zero;

		  for (size_t m = 0 ; m < nt ; m++)
		    {
		      Fsum = vadd(Fsum, F[k + n2 * m]);

		      if (GET_FLAG(flag[k + n2 * m], PARTICLE_STALE))
			SET_FLAG(p[k].flag, PARTICLE_STALE);
		    }

		  p[k].F = Fsum;
		}

#ifdef DUMP_THREAD_DATA

#define THREAD_DATA "thread.dat"

	      FILE *st = fopen(THREAD_DATA, "w");

	      for (size_t k = 0 ; k < n2 ; k++)
		{
		  vec_t F = p[k].F;
		  fprintf(st, "%i %f %f\n", (int)(p[k].flag), F.x, F.y);
		}

	      fclose(st);

	      log_info("thread data in %s, terminating", THREAD_DATA);
	      return ERROR_OK;
#endif
              /*
                finally add the containment field, this is like the body
                field, but most of it can be calculated once at the start
                of processing, then cached in a bilinear approximant which
                we evaluate below; this does need to be muliplied by the
                charge schedule (which is time varining)
              */

              for (size_t k = 0 ; k < n2 ; k++)
                {
                  vec_t Fc, v = p[k].v;
                  int err = contain_eval(opt->contain, v.x, v.y, &Fc);

                  switch (err)
                    {
                    case ERROR_OK:
                      p[k].F = vadd(vscale(sched.charge, Fc), p[k].F);
                    case ERROR_NODATA:
                      break;
                    default:
                      log_error("containment for arrow %zi", k);
                      return err;
                    }
                }
            }

	  /*
            this implements the leapfrog method commonly used
            in molecular dynamics

  	       v(t+dt/2) = v(t-dt/2) + a(t) dt
	       x(t+dt)   = x(t) + v(t+dt/2) dt

	    here x, v, a are the position, velocity, acceleration;
	    our struct uses different conventions.
	  */

	  for (size_t k = 0 ; k < n2 ; k++)
	    {
	      /*
                Scale invariant viscosity - we originally had viscous force
                F1 = Cd v = O(L), but this force should be O(L^2) so that
                the acceleration produced by it is O(L). So we multiply the
                viscous force by the mass (which is proportional to L), which
                could be interpreted as the viscous force being proportional
                to the area presented to the medium in real-world physics

                FIXME - write this in terms of the acceleration and so remove
                the mass mult/div
	      */

              double Cd = 14.5;
              vec_t F = vadd(vscale(-Cd * p[k].mass, p[k].dv), p[k].F);
              p[k].dv = vadd(vscale(dt / p[k].mass, F), p[k].dv);
              p[k].v = vadd(vscale(dt, p[k].dv), p[k].v);
            }

	  /* reset the physics */

	  for (size_t k = 0 ; k < n2 ; k++)
	    set_mq(p + k, sched.mass, sched.charge);
	}

      /* back in the main iteration */

      /*
	 Mark those with overclose neighbours, here we

	 - for each internal particle find the minimal pw-distance from
           amongst its neighbours
	 - sort by this value and take the top 2n
	 - create the intersection graph from this 2n and ensure there are
           no intersections
	 - take the top n of the remander and mark those as stale

         The number so marked is stored in 'nocl'
      */

      size_t nocl = 0;

      if ((n2 > 0) && (sched.dmax > 0) && (sched.rd > 0.0))
	{
          pw_t pw[n2];

          for (size_t j = 0 ; j < n2 ; j++)
            {
              pw[j].d = INFINITY;
	      pw[j].id = j;
            }

          for (size_t j = 0 ; j < (size_t)nedge ; j++)
            {
              size_t
                id0 = edge[2 * j],
                id1 = edge[2 * j + 1];

              vec_t rAB = vsub(p[id1].v, p[id0].v);
              double x;

              contact_mt(rAB, p[id0].M, p[id1].M, &x, NULL);

              if (x < 0)
		{
		  pw_error(rAB, p[id0], p[id1]);
		  continue;
		}

              double d = sqrt(x);

	      /*
		only attach to the smaller id - quick hack till we implement
		the non-interesct subset check

                FIXME - what on Earth does this mean?
	      */

              {
                double d0 = pw[id0].d;
                pw[id0].d = MIN(d, d0);
              }
            }

	  qsort(pw, n2, sizeof(pw_t), pwcomp);

	  double r = pw[0].d;

          for (size_t j = 0 ;
	       (r < sched.rd) && (nocl < sched.dmax) && (j < n2) ;
	       r = pw[++j].d)
            {
	      SET_FLAG(p[pw[j].id].flag, PARTICLE_STALE);
	      nocl++;
            }
	}

      /*
        re-evaluate the metric tensor at the ellipse positions, we may
        find that the new position is outside the field, which is OK if
        we've escaped from the domain, but not if we haven't (suggests
        a defective field).  We count those in nfx and write to file if
        so requested
      */

      size_t nfx = 0;

      for (size_t j = 0 ; j < n2 ; j++)
	{
	  if (GET_FLAG(p[j].flag, PARTICLE_STALE))
            continue;

	  switch (err = ellipse_mt_eval(opt->mt, p[j].v, &(p[j].M)))
	    {
	      ellipse_t E;

	    case ERROR_OK:
	      if (mat_ellipse(&(p[j].M), &E) != 0)
                {
                  log_error("metric tensor approximant not positive definite");
                  return ERROR_NODATA;
                }
	      p[j].major = E.major;
	      p[j].minor = E.minor;
	      break;

	    case ERROR_NODATA:
              if (st->escaped != NULL)
                {
                  fprintf(st->escaped,
                          "%zu,%f,%f,%s,%s\n",
                          i,
                          p[j].v.x, p[j].v.y,
                          "true",
                          domain_inside(opt->dom, p[j].v) ? "false" : "true");
                }
	      SET_FLAG(p[j].flag, PARTICLE_STALE);
              nfx++;
	      break;

	    default: return err;
	    }
	}

      /*
        mark domain escapees, there will be very few of these since they
        with be caught by the metric-tensor nodata case above
      */

      for (size_t j = 0 ; j < n2 ; j++)
	{
	  if (GET_FLAG(p[j].flag, PARTICLE_STALE))
            continue;

	  if (! domain_inside(opt->dom, p[j].v))
            {
              if (st->escaped != NULL)
                {
                  fprintf(st->escaped,
                          "%zu,%f,%f,%s,%s\n",
                          i,
                          p[j].v.x, p[j].v.y,
                          "false",
                          "true");
                }
              SET_FLAG(p[j].flag, PARTICLE_STALE);
            }
        }

      /*
        Sort to get stale particles at the end. Note that this destroys
        the validity of the neighbour network so must be outside the inner
        loop.
      */

      qsort(p, n2, sizeof(particle_t), ptcomp);

      /* adjust n2 to discard stale particles */

      while (n2 && GET_FLAG(p[n2 - 1].flag, PARTICLE_STALE))
        n2--;

      if (n2 == 0)
	{
	  log_error("all symbols lost, bad topology?");
	  return ERROR_NODATA;
	}

      /* recreate neighbours for the next cycle */

      free(edge); edge = NULL;

      if ((err = neighbours(p, n2, &edge, &nedge)) != ERROR_OK)
	{
	  log_error("failed to generate neighbour mesh");
	  return err;
	}

      /* kinetic energy */

      double ke = 0.0;

      for (size_t j = 0 ; j < n2 ; j++)
        ke += p[j].mass * vabs2(p[j].dv);

      ke = ke / (2.0 * n2);

      /* handle db drop wait */

      if (i == wait.iter)
	{
	  wait.kedB = 10*log10(ke);

#ifdef WAIT_DEBUG
	  log_info("waiting for %f", wait.kedB - wait.drop);
#endif
	}
      else if (i > wait.iter)
	{
	  if (10 * log10(ke) < wait.kedB - wait.drop)
	    {
	      wait.done = true;

#ifdef WAIT_DEBUG
	      log_info("waiting over");
#endif
	    }
	}

      /* ellipse area and proportion of domain */

      double eA = 0.0;

      for (size_t j = 0 ; j < n2 ; j++)
        eA += p[j].minor * p[j].major;

      eA *= M_PI;

      eprop = eA / dA;

      /* edges per point */

      double epp = ((double)nedge) / n2;

      /* user statistics */

      log_info("%3zi %5zi %3zi %3zi %5i %6.3f %7.2f %5.3f",
               i, n2, nocl, nfx, nedge, epp,
               (ke > 0 ? 10 * log10(ke) : -INFINITY),
               eprop);

      /* breakouts */

      switch (opt->plot.place.adaptive.breakout)
	{
	case break_super:
	  if (T > BREAK_SUPER)
	    {
	      log_info("[break during superposition]");
	      goto output;
	    }
	  break;

	case break_midclean:
	  if (T > BREAK_MIDCLEAN)
	    {
	      log_info("[break in middle of cleaning]");
	      goto output;
	    }
	  break;

	case break_postclean:
	  if ((T > BREAK_POSTCLEAN) && (nocl == 0))
	    {
	      log_info("[break after cleaning]");
	      goto output;
	    }
	  break;

	default:
	  break;
	}

#ifdef HAVE_SIGNAL_H

      if (exitflag)
	{
	  if (sigaction(SIGINT, &oldact, NULL) == -1)
	    log_warn("failed to restore signal handler");

	  goto output;
	}

#endif

    }

  log_info(hline);

 output:

#ifdef PTHREAD_FORCES

  if (set_terminate_status(&(tshared.mutex), &(tshared.terminate), true) != 0)
    return 0;

  err = pthread_barrier_wait( &(tshared.barrier[0]) );
  if ((err != 0) && (err != PTHREAD_BARRIER_SERIAL_THREAD) )
    {
      log_error("error on barrier 0 wait: %s", strerror(err));
      return ERROR_PTHREAD;
    }

#endif

  /* report results */

#define EDENS_UNDERFULL 0.9
#define EDENS_OVERFULL 1.2
#define EDENS_DEFECT 0.2

  {
    double
      earat = eprop / EPACKOPT,
      edens = ((double)n2) / no;

    log_info("ellipse area ratio %.0f%%, density %.0f%%",
             100.0 * earat, 100.0 * edens);

    if (edens < EDENS_UNDERFULL)
      log_info("looks underfull, try larger overfill");

    if ((edens > EDENS_OVERFULL) || (edens - earat > EDENS_DEFECT))
      log_info("looks overfull, try more iterations");
  }

  /* encapulate the network data in array of nbr_t for output */

  nbr_t *nbrs = nbrs_populate(nedge, edge, n2, p);

  if (!nbrs) return ERROR_BUG;

  N->n = nedge;
  N->v = nbrs;

  free(edge);

  /* reallocate the output arrow array and transfer data from p */

  if (n2 > A->n)
    {
      arrow_t *Av;

      if ((Av = realloc(A->v, n2 * sizeof(arrow_t))) == NULL)
	return ERROR_MALLOC;

      A->v = Av;
    }

  for (size_t i = 0 ; i < n2 ; i++)
    {
      /* coverity[deref_parm] */
      arrow_t *arrow = &(A->v[i]);
      arrow->centre = p[i].v;
      complete_arrow(opt->evaluate, arrow);
    }

  A->n = n2;

  free(p);

#ifdef PTHREAD_FORCES

  /* harvest workers as they exit */

  for (size_t k = 0 ; k < nt ; k++)
    {
      err = pthread_join(thread[k], NULL);
      if (err)
	{
	  log_error("joining thread %zi: %s", k, strerror(err));
	  return ERROR_PTHREAD;
	}
    }

  /* destroy barriers */

  for (size_t k = 0 ; k < 2 ; k++)
    {
      err = pthread_barrier_destroy(tshared.barrier + k);
      if (err)
	{
	  log_error("barrier %zi destroy: %s", k, strerror(err));
	  return ERROR_PTHREAD;
	}
    }

#endif

  return ERROR_OK;
}

int dim2(dim2_opt_t *opt, arrows_t *A, nbrs_t *N)
{
  dim2_streams_t st = { NULL };

  const char *hist_path = opt->plot.place.adaptive.histogram;

  if (hist_path != NULL)
    {
      st.histogram = fopen(hist_path, "w");
      if (st.histogram == NULL)
        log_warn("failed to open %s for writing", hist_path);
    }

  const char *esc_path = opt->plot.file.escaped.path;

  if (esc_path != NULL)
    {
      st.escaped = fopen(esc_path, "w");
      if (st.escaped == NULL)
        log_warn("failed to open %s for writing", esc_path);
    }

  int err = dim2_streams(opt, &st, A, N);

  if (st.histogram != NULL)
    fclose(st.histogram);

  if (st.escaped != NULL)
    fclose(st.escaped);

  return err;
}

/* return a nbrs array populated from the edge list */

static nbr_t* nbrs_populate(size_t nedge, uint32_t *edge, size_t np, particle_t *p)
{
  nbr_t *nbrs;

  if ((nbrs = malloc(nedge*sizeof(nbr_t))) == NULL)
    return NULL;

  for (size_t i = 0 ; i < nedge ; i++)
    {
      uint32_t id[2];

      for (size_t j = 0 ; j < 2 ; j++)
	{
	  uint32_t idj = edge[2 * i + j];

	  if (idj >= np)
	    {
	      log_warn("edge (%zi, %zi) id of %i", i, j, idj);
	      goto cleanup;
	    }

	  id[j] = idj;
	}

      nbrs[i].a.id = id[0];
      nbrs[i].b.id = id[1];

      nbrs[i].a.v = p[id[0]].v;
      nbrs[i].b.v = p[id[1]].v;
    }

  return nbrs;

 cleanup:

  free(nbrs);

  return NULL;
}

/*
  Generate neighbour pairs -- this is rather complex as it marshals
  lots of resources (which we don't want to leak) and at the top of
  that, a search on an R-tree via the stack_ids callback.

  So we have a series on functions building up the neighbours state
  and releasing those resources on the way down
*/

typedef struct
{
  rtree_id_t id;
  gstack_t *stack;
} stack_ids_context_t;

static int stack_ids(rtree_id_t id2, void *arg)
{
  stack_ids_context_t *context = arg;
  rtree_id_t id1 = context->id;

  if (id1 < id2)
    {
      uint32_t pair[2] = { id1, id2 };

      if (gstack_push(context->stack, pair) != 0)
        return 1;
    }

  return 0;
}

typedef struct
{
  struct
  {
    size_t n;
    particle_t *data;
  } particle;
  struct
  {
    size_t n;
    uint32_t *data;
  } edge;
  gstack_t *stack;
  rtree_t *rtree;
  rtree_coord_t *rects;
} neighbours_state_t;

static int neighbours6(neighbours_state_t *state)
{
  size_t np = state->particle.n;
  rtree_coord_t *rects = state->rects;
  rtree_t *rtree = state->rtree;
  gstack_t *stack = state->stack;

  for (size_t i = 0 ; i < np ; i++)
    if (rtree_add_rect(rtree, i, rects + 4 * i) != 0)
      return ERROR_BUG;

  stack_ids_context_t context = { .stack = stack };

  for (size_t i = 0 ; i < np ; i++)
    {
      context.id = i;
      if (rtree_search(rtree, rects + 4 * i, stack_ids, &context) != 0)
        return ERROR_BUG;
    }

  return ERROR_OK;
}

#define RECT_MARGIN 0.05

static int neighbours5(neighbours_state_t *state)
{
  size_t np = state->particle.n;
  particle_t *p = state->particle.data;
  size_t rects_size = sizeof(rtree_coord_t) * 4 * np;
  rtree_coord_t *rects;

  if ((rects = malloc(rects_size)) == NULL)
    return ERROR_MALLOC;

  double K = 1 + RECT_MARGIN;

  for (size_t i = 0 ; i < np ; i++)
    {
      vec_t v = p[i].v;
      double
        dx = sqrt(mxmax(p[i].M)) * K,
        dy = sqrt(mymax(p[i].M)) * K;
      rtree_coord_t *rect = rects + 4 * i;

      rect[0] = v.x - dx;
      rect[1] = v.y - dy;
      rect[2] = v.x + dx;
      rect[3] = v.y + dy;
    }

  state->rects = rects;

  int err = neighbours6(state);

  free(rects);
  state->rects = NULL;

  return err;
}

static int neighbours4(neighbours_state_t *state)
{
  rtree_t *rtree;

  if ((rtree = rtree_new(2, RTREE_DEFAULT)) == NULL)
    return ERROR_MALLOC;

  state->rtree = rtree;

  int err = neighbours5(state);

  rtree_destroy(rtree);
  state->rtree = NULL;

  return err;
}

static int neighbours3(neighbours_state_t *state)
{
  int err = neighbours4(state);

  if (err == ERROR_OK)
    {
      gstack_t *stack = state->stack;
      size_t nedge = gstack_size(stack);
      uint32_t *edge;

      if ((edge = malloc(2 * sizeof(uint32_t) * nedge)) != NULL)
        {
          for (size_t i = 0 ; i < nedge ; i++)
            if (gstack_pop(stack, edge + 2 * i) != 0)
              {
                err = ERROR_BUG;
                break;
              }

          if (err == 0)
            {
              state->edge.n = nedge;
              state->edge.data = edge;
            }
          else
            free(edge);
        }
      else
        err = ERROR_MALLOC;
    }

  return err;
}

static int neighbours2(neighbours_state_t *state)
{
  size_t np = state->particle.n;
  gstack_t *stack;

  if ((stack = gstack_new(2 * sizeof(uint32_t), np * 6, np)) == NULL)
    return ERROR_MALLOC;

  state->stack = stack;

  int err = neighbours3(state);

  gstack_destroy(stack);
  state->stack = NULL;

  return err;
}

static int neighbours(particle_t *p, size_t np, uint32_t **pe, uint32_t *pne)
{
  neighbours_state_t state = { .particle = { .n = np, .data = p } };

  int err = neighbours2(&state);

  if (err == ERROR_OK)
    {
      *pne = state.edge.n;
      *pe = state.edge.data;
    }

  return err;
}

/*
  subdivide a range 0..ne into nt subranges specified by offset and size.
  eg 0..20 by 2 -> 0..10, 11..20
*/

static int subdivide(size_t nt, size_t ne, size_t *off, size_t *size)
{
  if ((nt < 1) || (ne < 1))
    return 1;

  size_t m = ne / nt;

  for (size_t i = 0 ; i < nt - 1 ; i++)
    {
      off[i] = i * m;
      size[i] = m;
    }

  off[nt - 1] = (nt - 1) * m;
  size[nt - 1] = ne - (nt - 1) * m;

  return 0;
}

#ifdef PTHREAD_FORCES

static int get_terminate_status(pthread_mutex_t *mutex, bool *pterm, bool *pval, int id)
{
  int err;

  err = pthread_mutex_lock(mutex);
  if (err)
    {
      log_error("on terminate check mutex for thread %i: %s",
                id, strerror(err));
      return 1;
    }

  *pval = *pterm;

  err = pthread_mutex_unlock(mutex);
  if (err)
    {
      log_error("error on terminate check mutex for thread %i: %s",
                id, strerror(err));
      return 1;
    }

  return 0;
}

static int set_terminate_status(pthread_mutex_t *mutex, bool *pterm, bool value)
{
  int err;

  if ((err = pthread_mutex_lock(mutex)) != 0)
    {
      log_error("terminate set mutex: %s", strerror(err));
      return 1;
    }

  *pterm = value;

  if ((err = pthread_mutex_unlock(mutex)) != 0)
    {
      log_error("terminate set mutex: %s", strerror(err));
      return 1;
    }

  return 0;
}

/* FIXME use a sensible return value here */

static void* forces_worker(void *arg)
{
  tdata_t *pt = arg;
  int err, id = pt->id;
  bool terminate;

  while (1)
    {
      err = pthread_barrier_wait(&(pt->shared->barrier[0]));
      if ((err != 0) && (err != PTHREAD_BARRIER_SERIAL_THREAD))
	{
	  log_error("error at barrier 0 wait for thread %i: %s",
                    id, strerror(err));
	  return NULL;
	}

      if ((get_terminate_status(&(pt->shared->mutex),
				&(pt->shared->terminate),
				&terminate, id) != 0) || terminate )
	return NULL;

      forces(pt);

      err = pthread_barrier_wait(&(pt->shared->barrier[1]));
      if ((err != 0) && (err != PTHREAD_BARRIER_SERIAL_THREAD))
	{
	  log_error("error at barrier 1 wait for thread %i: %s",
                    id, strerror(err));
	  return NULL;
	}
    }
}

#endif

/*
  this accumulates the forces for the edges g.edge[t.off] ...
  g.edge[t.off + t.size - 1] and puts the results in t.F, a private
  vector array (so no mutex required).
*/

static void* forces(tdata_t *pt)
{
  tdata_t t = *pt;
  tshared_t s = *(t.shared);

  for (size_t i = 0 ; i < t.size ; i++)
    {
      int
        k = i + t.off,
        idA = s.edge[2 * k],
        idB = s.edge[2 * k + 1];
      vec_t
        rAB = vsub(s.p[idB].v, s.p[idA].v),
        uAB;
      double d2;

      contact_mt(rAB, s.p[idA].M, s.p[idB].M, &d2, &uAB);

      if (d2 < 0)
	{
	  pw_error(rAB, s.p[idA], s.p[idB]);
	  continue;
	}

      double
        d = sqrt(d2),
        dV = potential_dV(d, s.rt),
        f = dV * s.p[idA].charge * s.p[idB].charge * SCALE_BODY;

      t.F[idA] = vadd(vscale(f, uAB), t.F[idA]);
      t.F[idB] = vadd(vscale(-f, uAB), t.F[idB]);
    }

  return NULL;
}

/*
  vfplot.c
  Copyright (c) J.J. Green 2007, 2022

  Main plotting routine
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <time.h>

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_SYS_STAT_H
#include <sys/stat.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_SYS_RESOURCE_H
#include <sys/resource.h>
#endif

#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

#include <log.h>
#include <stats.h>

#include <plot/plot.h>
#include <plot/evaluate.h>
#include <plot/adaptive.h>
#include <plot/hedgehog.h>
#include <plot/csv-write.h>
#include <plot/gstate.h>
#include <plot/domain.h>
#include <plot/domain-geojson.h>
#include <plot/bbox.h>
#include <plot/nbrs.h>
#include <plot/autoscale.h>

#include <vf/type.h>
#include <vf/circular.h>
#include <vf/cylinder.h>
#include <vf/electro.h>
#include <vf/smoska.h>
#include <vf/field.h>

#include "vfplot.h"


static int vfplot_circular(opt_t*);
static int vfplot_cylinder(opt_t*);
static int vfplot_electro2(opt_t*);
static int vfplot_electro3(opt_t*);
static int vfplot_smoska(opt_t*);

static int vfplot_domain(vf_methods_t*, void*, opt_t*);
static int vfplot_generic(domain_t*, vfun_t*, cfun_t*, void*, opt_t*);

#ifdef HAVE_GETTIMEOFDAY

static int timeval_subtract(struct timeval *res,
			    const struct timeval*,
			    const struct timeval*);

#endif

/*
  see if we are running a test-field, is so call the appropriate
  wrapper function (which will call vfplot_generic, and then vfplot)
*/

int vfplot(opt_t *opt)
{
  int err = ERROR_BUG;
  stats_t stats;

  stats.version = PACKAGE_VERSION;

#ifndef HAVE_GETRUSAGE

  clock_t c0, c1;
  c0 = clock();

#endif

#ifdef HAVE_GETTIMEOFDAY

  struct timeval tv0;
  gettimeofday(&tv0, NULL);

#endif

#ifdef HAVE_PTHREAD_H

  log_info("using %i thread%s",
           opt->plot.threads,
           (opt->plot.threads == 1 ? "" : "s"));

#endif

  if (opt->test != test_none)
    {
      const char *description = NULL;
      int (*test_function)(opt_t*) = NULL;

      switch (opt->test)
	{
	case test_circular:
	  description = "circular";
	  test_function = vfplot_circular;
	  break;
	case test_cylinder:
	  description = "cylindrical flow";
	  test_function = vfplot_cylinder;
	  break;
	case test_electro2:
	  description = "two-point electrostatic";
	  test_function = vfplot_electro2;
	  break;
	case test_electro3:
	  description = "three-point electrostatic";
	  test_function = vfplot_electro3;
	  break;
	case test_smoska:
	  description = "Smolik-Skala";
	  test_function = vfplot_smoska;
	  break;
	default:
          log_error("no such test-field");
	  return ERROR_BUG;
	}

      log_info("%s test field", description);
      err = test_function(opt);
    }
  else
    {
      {
        int absent = 0;

        for (size_t i = 0 ; i < opt->input.n ; i++)
          {
            const char *path = opt->input.paths[i];
            if (access(path, R_OK) != 0)
              {
                log_error("file %s: %s", path, strerror(errno));
                absent++;
              }
          }

        if (absent != 0)
          return ERROR_READ_OPEN;
      }

      log_info("reading field from");

      for (size_t i = 0 ; i < opt->input.n ; i++)
        log_info("  %s", opt->input.paths[i]);

      field_t *field =
        field_read(opt->input.format,
                   opt->input.align,
                   opt->input.n,
                   opt->input.paths,
                   opt->input.grid,
                   opt->input.range);

      if (field == NULL)
	{
	  log_error("failed to read field");
	  return ERROR_READ_OPEN;
	}

      err = vfplot_domain(&field_methods, field, opt);

      field_destroy(field);
    }

  stats.size_type = size_absent;

#ifdef HAVE_STAT

  if (err == ERROR_OK)
    {
      const char *path = opt->plot.file.output.path;

      if (path != NULL)
        {
          /*
            st_size is of type off_t, which is defined by POSIX to
            be a signed integer type (size is not prescribed), in
            glibc, it is a long (and in practice it will be a few
            thousand at most
          */

          struct stat sb;

	  if (stat(path, &sb) != 0)
            log_warn("problem with %s: %s", path, strerror(errno));
          else
            {
              log_info("wrote %li bytes to %s", (long)sb.st_size, path);

              stats.size_type = size_present;
              stats.size = sb.st_size;
            }
        }
    }

#endif

#ifdef HAVE_GETRUSAGE

  /* detailed stats on POSIX systems */

  struct rusage usage;

  if (getrusage(RUSAGE_SELF, &usage) == 0)
    {
      double
        user = usage.ru_utime.tv_sec + (double)usage.ru_utime.tv_usec / 1e6,
        sys = usage.ru_stime.tv_sec + (double)usage.ru_stime.tv_usec / 1e6;
      log_info("CPU time %.3f s (user) %.3f s (system)", user, sys);

      stats.cpu_type = cpu_rusage;
      stats.cpu.rusage.user = user;
      stats.cpu.rusage.sys = sys;
    }
  else
    {
      log_warn("no usage stats; error %s", strerror(errno));
      stats.cpu_type = cpu_error;
    }

#else

  c1 = clock();
  double wall = ((double)(c1 - c0)) / (double)CLOCKS_PER_SEC;
  log_info("CPU time %.3f s", wall);

  stats.cpu_type = cpu_wall;
  stats.cpu.wall = wall;

#endif

#ifdef HAVE_GETTIMEOFDAY

  struct timeval tv1, dtv;

  gettimeofday(&tv1, NULL);
  timeval_subtract(&dtv, &tv1, &tv0);

  log_info("elapsed time %ld.%03ld s", dtv.tv_sec, dtv.tv_usec / 1000);

  stats.elapsed_type = elapsed_present;
  stats.elapsed = dtv.tv_sec + dtv.tv_usec * 1e-6;

#else

  stats.elapsed_type = elapsed_absent;

#endif

  if (opt->statistics.path != NULL)
    {
      const char *path = opt->statistics.path;
      FILE *stream;

      if ((stream = fopen(path, "w")) != NULL)
        {
          if (stats_write(stream, &stats) != 0)
            {
              log_warn("failed writing statistics to %s", path);
              err = ERROR_BUG;
            }

          fclose(stream);
        }
      else
        {
          log_warn("failed to open %s: %s", path, strerror(errno));
          err = ERROR_WRITE_OPEN;
        }

#ifdef HAVE_UNLINK

      if (err != ERROR_OK)
        {
          if (unlink(path) != 0)
            log_warn("failed delete of of %s: %s", path, strerror(errno));
        }

#endif

    }

  return err;
}

#ifdef HAVE_GETTIMEOFDAY

static int timeval_subtract(struct timeval *res,
			    const struct timeval *t2,
			    const struct timeval *t1)
{
  long diff =
    (t2->tv_usec + 1000000 * t2->tv_sec) -
    (t1->tv_usec + 1000000 * t1->tv_sec);

  res->tv_sec = diff / 1000000;
  res->tv_usec = diff % 1000000;

  return (diff < 0);
}

#endif

#define DUMP_X_SAMPLES 128
#define DUMP_Y_SAMPLES 128

static int vfplot_generic(domain_t *dom,
                          vfun_t *fv, cfun_t *fc,
                          void *field, opt_t *opt)
{

  if (opt->dump.vectors.path)
    {
      log_info("dumping vector field to %s", opt->dump.vectors.path);
      int err =
        csv_write(opt->dump.vectors.path,
                  dom, fv, field,
                  DUMP_X_SAMPLES,
                  DUMP_Y_SAMPLES);

      if (err != ERROR_OK)
        {
          log_warn("failed to write vector field");
          return err;
        }
    }

  if (opt->dump.domain.path)
    {
      int err = domain_write(opt->dump.domain.path, dom);
      if (err != ERROR_OK)
	return err;
    }

  int err = ERROR_BUG;
  arrows_t A = { .n = 0, .v = NULL};
  nbrs_t N = { .n = 0, .v = NULL};

  if (opt->state.action == state_read)
    {
      const char *path = opt->state.path;
      gstate_t state = {0};
      FILE *stream;

      log_info("reading state from %s", path);

      if ((stream = fopen(path, "r")) == NULL)
        {
          log_error("error reading from %s: %s", path, strerror(errno));
          return ERROR_READ_OPEN;
        }

      err = gstate_read(stream, &state);

      if (fclose(stream) != 0)
        {
          log_error("error closing %s: %s", path, strerror(errno));
          return ERROR_BUG;
        }

      if (err != ERROR_OK)
        {
          log_warn("failed read of %s", path);
          return err;
        }

      A.n = state.arrows.n;
      A.v = state.arrows.v;
      N.n = state.nbrs.n;
      N.v = state.nbrs.v;

      log_info("read %zi arrows, %zi neighbours", A.n, N.n);
    }
  else
    {
      switch (opt->place)
	{
	case place_hedgehog:
	  err = plot_hedgehog(dom, fv, fc, field, &(opt->plot), &A);
	  break;
	case place_adaptive:
	  err = plot_adaptive(dom, fv, fc, field, &(opt->plot), &A, &N);
	  break;
	default:
	  err = ERROR_BUG;
	}
    }

  if (err != ERROR_OK)
    goto cleanup;

  if (opt->state.action == state_write)
    {
      gstate_t state;

      state.arrows.n = A.n;
      state.arrows.v = A.v;
      state.nbrs.n = N.n;
      state.nbrs.v = N.v;

      const char *path = opt->state.path;
      FILE* stream;

      if ((stream = fopen(path, "w")) == NULL)
        {
          log_error("error writing to %s: %s", path, strerror(errno));
          err = ERROR_WRITE_OPEN;
          goto cleanup;
        }

      err = gstate_write(stream, &state);

      if (fclose(stream) != 0)
        {
          log_error("error closing %s: %s", path, strerror(errno));
          err = ERROR_BUG;
          goto cleanup;
        }

      if (err != ERROR_OK)
	{
	  log_error("failed write of %s", path);
	  goto cleanup;
	}

      log_info("wrote state file to %s", path);
    }

  if (A.n > 0)
    {
      if (A.v != NULL)
        {
          err = plot_output(dom, &A, &N, &(opt->plot));
          if (err != ERROR_OK)
            log_error("failed plot output");
        }
      else
        {
          err = ERROR_BUG;
          log_error("NULL arrows array");
        }
    }
  else
    {
      if (A.v != NULL)
        {
          err = ERROR_BUG;
          log_error("non-NULL arrows array");
        }
      else
        {
          err = ERROR_NODATA;
          log_warn("no data in graphics state file");
        }
    }

 cleanup:

  free(N.v);
  free(A.v);

  return err;
}

static int vfplot_domain(vf_methods_t *methods, void *field, opt_t *opt)
{
  domain_t *dom;

  if (opt->domain.path)
    dom = domain_read(opt->domain.path);
  else
    dom = methods->domain(field);

  if (dom == NULL)
    {
      log_error("no domain");
      return ERROR_BUG;
    }

  int err;
  bbox_t bb;

  if ((err = domain_bbox(dom, &bb)) != ERROR_OK)
    return err;

  log_info("domain extent [%.2f, %.2f] x [%.2f, %.2f]",
           bb.x.min, bb.x.max,
           bb.y.min, bb.y.max);

  plot_opt_t *plot_opt = &(opt->plot);

  if ((err = plot_opt_init(bb, plot_opt)) != ERROR_OK)
    return err;

  vfun_t *vf = methods->vector;
  cfun_t *vc = methods->curvature;
  sfun_t *vs = methods->scale;

  if ((err = autoscale(dom, vf, vc, vs, field, plot_opt)) == ERROR_OK)
    {
      if ((err = vfplot_generic(dom, vf, vc, field, opt) != ERROR_OK))
        log_error("failed plot");
    }
  else
    log_error("failed autoscale");

  domain_destroy(dom);

  return err;
}

static int vfplot_circular(opt_t *opt)
{
  circular_field_t field = { .width = 1, .height = 1, .scale = 0 };

  return vfplot_domain(&circular_methods, &field, opt);
}

static int vfplot_cylinder(opt_t *opt)
{
  cylinder_field_t field = {
    .x = 0.0,
    .y = -0.3,
    .radius = 0.15,
    .speed = 1.0,
    .gamma = 5.0,
    .scale = 0
  };

  return vfplot_domain(&cylinder_methods, &field, opt);
}

static int vfplot_electro2(opt_t *opt)
{
  charge_t c[2] = {
    { 1,  0.4,  0.4},
    {-2, -0.4, -0.4}
  };
  electro_field_t field = { .n = 2, .charge = c, .scale = 0 };

  return vfplot_domain(&electro_methods, &field, opt);
}

static int vfplot_electro3(opt_t *opt)
{
  charge_t c[3] = {
   { 1.0, -0.4, -0.2},
   { 1.0,  0.0,  0.4},
   {-1.0,  0.4, -0.2}
  };
  electro_field_t field = { .n = 3, .charge = c, .scale = 0 };

  return vfplot_domain(&electro_methods, &field, opt);
}

static int vfplot_smoska(opt_t *opt)
{
  smoska_field_t field = { .scale = 0 };

  return vfplot_domain(&smoska_methods, &field, opt);
}

void vfplot_clean(opt_t *opt)
{
  free((field_grid_t*)opt->input.grid);
  free((field_range_t*)opt->input.range);
}

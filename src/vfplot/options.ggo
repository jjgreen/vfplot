package "vfplot"
purpose "Plot two-dimensional vector fields"
description
"For options which take a TYPE argument, use the value 'list' to see the
available values that TYPE can take."
args "--unnamed-opts --no-help --no-version --default-optional"


section "General" sectiondesc=""

option "help" h
"print options and exit"

option "threads" j
"number of threads, or 0 for number of CPUs"
int default="0"

option "log-path" L
"log to PATH"
string
typestr="PATH"

option "log-level" -
"the LEVEL at which to log"
string default="DEBUG"
typestr="LEVEL"

option "version" V
"print version and exit"

option "verbose" v
"verbose"
flag off


section "Plot type, input, output"
sectiondesc=""

option "format" F
"input data-file TYPE"
string default="auto"
typestr="TYPE"

option "grid-range" -
"input grid x and y extent"
string
typestr="LENGTHS"

option "grid-size" -
"input grid i and j extent"
string
typestr="INTS"

option "output" o
"output plot to PATH"
string
typestr="PATH"

option "output-format" O
"output format"
string default="eps"
typestr="TYPE"

option "pixel" -
"input grid is pixel aligned"
flag off

option "placement" p
"arrow placement TYPE"
string default="adaptive"
typestr="TYPE"

option "test" t
"plot test field NAME rather than data"
string
typestr="NAME"


section "Plot features"
sectiondesc=""

option "crop" c
"crop output image by LENGTHS"
string
typestr="LENGTHS"

option "domain" d
"read field domain from PATH"
string
typestr="PATH"

option "domain-pen" D
"draw domain with PEN"
string
typestr="PEN"

option "width" w
"width of output image"
string default="4i"
typestr="LENGTH"

option "height" W
"height of output image"
string
typestr="LENGTH"


section "Arrows"
sectiondesc="the type and appearance of the plotted arrows\n"

option "aspect" a
"arrow length/width ratio"
float
typestr="FLOATS"

option "fill" f
"arrow fill"
string
typestr="FILL"

option "head" H
"arrow-head length/width ratios"
string default="1.7/2.2"
typestr="FLOATS"

option "length" l
"arrow-length min/max"
string default="1m/10c"
typestr="LENGTHS"

option "number" n
"number of arrows"
int default="200"

option "pen" P
"draw arrow with PEN"
string default="0.5m"
typestr="PEN"

option "scale" s
"arrow scale"
float default="1"

option "sort" -
"arrow sorting strategy"
string default="none"
typestr="TYPE"

option "symbol" S
"use the TYPE symbol"
string default="triangle"
typestr="TYPE"

section "Adaptive mode"
sectiondesc="arrow placement by a packing of the bounding ellipses\n"

option "ellipse" E
"plot bounding ellipses"
flag off

option "ellipse-pen" -
"pen for drawing ellipses"
string default="0.3m"
typestr="PEN"

option "ellipse-fill" -
"use FILL in drawing ellipses"
string
typestr="FILL"

option "iterations" i
"number of iterations"
string default="40/10"
typestr="INTS"

option "overfill" -
"initial overfill factor"
float default="2.0"

option "ke-drop" k
"wait till KE drops by dB"
float default="0.0"

option "margin" m
"min/rate of arrow padding"
string default="4m/3m/0.5"
typestr="LENGTHS/FLOAT"

option "network-pen" -
"draw neighbour network with PEN"
string
typestr="PEN"

option "timestep" -
"iteration timestep"
float default="0.01"


section "Advanced"
sectiondesc="mainly for development\n"

option "animate" -
"create animation frames"
flag off

option "animate-dir" -
"location of animation frames"
string
typestr="DIR"

option "break" -
"terminate at breakpoint"
string
typestr="TYPE"

option "cache" -
"metric tensor cache size"
int default="128"

option "dump-vectors" -
"write vectors to PATH"
string
typestr="PATH"

option "dump-domain" -
"write domain to PATH"
string
typestr="PATH"

option "epsilon" e
"visual epsilon"
string default="0.3p"
typestr="LENGTH"

option "escaped" -
"write CSV of escaped ellipses to PATH"
string
typestr="PATH"

option "graphic-state" G
"use graphics state at PATH"
string
typestr="PATH"

option "histogram" -
"write histogram data to PATH"
string
typestr="PATH"

option "statistics" -
"write statistics to PATH"
string
typestr="PATH"

text "\nSee vfplot(1) for more details"

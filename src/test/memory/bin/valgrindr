#!/bin/sh

set -e

# simple test harness for valgrind, arguments for PROG are
# in the files in TESTS_DIR/*.argv, the basenames of those
# files are the "names" of the tests

PROG="$1"
VALGRIND="$2"
TESTS_DIR="$3"

echo "This is valgrindr"
echo "target: $PROG"
echo "test directory: $TESTS_DIR"

if [ ! -x $PROG ]
then
    echo "target $PROG not present, skipping"
    exit 0
fi

passes=0
failures=0

base_prog=$(basename "$PROG")

for path in "$TESTS_DIR"/*.argv
do
    args=$(cat "$path")
    base_test=$(basename "$path" .argv)
    set +e
    # shellcheck disable=SC2086 # $args must not be quoted
    ${VALGRIND} \
        --leak-check=full \
        --show-leak-kinds=all \
        --errors-for-leak-kinds=all \
        --show-reachable=yes \
        --error-exitcode=1 \
        --quiet \
        --track-origins=yes \
        --log-file="tmp/${base_prog}-${base_test}.log" \
        --gen-suppressions=all \
        --suppressions='config/suppressions' \
        "$PROG" $args > /dev/null
    status=$?
    set -e
    if [ $status -eq 0 ]
    then
        echo "- ${base_test}: OK"
        # shellcheck disable=SC2003 # expr is needed here
        passes=$(expr $passes + 1)
    else
        echo "- ${base_test}: FAIL"
        # shellcheck disable=SC2003 # expr is needed here
        failures=$(expr $failures + 1)
    fi
done

echo "passed: $passes"
echo "failed: $failures"
echo "done."

exit "$failures"

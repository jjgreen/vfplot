#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <vf/field-range.h>

#include "tests-vf-field-range.h"

CU_TestInfo tests_field_range[] = {
  {"valid, valid", test_field_range_valid_valid},
  {"valid, invalid", test_field_range_valid_invalid},
  CU_TEST_INFO_NULL
};

void test_field_range_valid_valid(void)
{
  field_range_t range = {
    .x = { .min = 0, .max = 1 },
    .y = { .min = 0, .max = 1 }
  };
  CU_ASSERT(field_range_valid(&range));
}

void test_field_range_valid_invalid(void)
{
  field_range_t
    range1 =
    {
      .x = { .min = 0, .max = 1 },
      .y = { .min = 1, .max = 0 }
    },
    range2 =
    {
      .x = { .min = 1, .max = 0 },
      .y = { .min = 0, .max = 1 }
    };

  CU_ASSERT_FALSE(field_range_valid(&range1));
  CU_ASSERT_FALSE(field_range_valid(&range2));
}

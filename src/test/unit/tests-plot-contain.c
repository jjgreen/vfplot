#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/contain.h>
#include <plot/error.h>

#include "tests-plot-contain.h"
#include "assert-vec.h"
#include "factory-domain.h"

CU_TestInfo tests_contain[] = {
  {"new", test_contain_new},
  {"eval", test_contain_eval},
  CU_TEST_INFO_NULL
};

/*
  The test-case here is a 60 x 60 square bounding x-asis aligned
  ellipses of majority 5 minority 5/3, we construct the ellipse_mt_t
  manually, rather than use metric_tensor_new which depends on
  how one creates an ellipse from an arrow.
*/

static ellipse_mt_t* ellipse_mt_build(void)
{
  bbox_t bbox = {{0, 60}, {0, 60}};
  size_t nx = 60, ny = 60;

  ellipse_mt_t *mt = bilinear_new(nx, ny, 3);
  CU_ASSERT_PTR_NOT_NULL_FATAL(mt);
  bilinear_bbox_set(mt, bbox);

  ellipse_t E = { .major = 5, .minor = 5.0 / 3, .theta = 0 };
  mat_t M;
  CU_ASSERT_EQUAL(ellipse_mat(&E, &M), ERROR_OK);

  double z[3] = { M.a, M.b, M.d };

  for (size_t i = 0 ; i < nx ; i++)
    for (size_t j = 0 ; j < ny ; j++)
      bilinear_z_set(mt, i, j, z);

  return mt;
}

static ellipse_major_t* ellipse_major_build(void)
{
  bbox_t bbox = {{0, 60}, {0, 60}};
  size_t nx = 60, ny = 60;

  ellipse_major_t *major = bilinear_new(nx, ny, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(major);
  bilinear_bbox_set(major, bbox);

  double z = 5;

  for (size_t i = 0 ; i < nx ; i++)
    for (size_t j = 0 ; j < ny ; j++)
      bilinear_z_set(major, i, j, &z);

  return major;
}

void test_contain_new(void)
{
  domain_t *dom = factory_domain_nested();
  ellipse_mt_t *mt = ellipse_mt_build();
  ellipse_major_t *major = ellipse_major_build();

  double max_length = 10;
  contain_t *contain = contain_new(dom, mt, major, max_length);
  CU_ASSERT_PTR_NOT_NULL_FATAL(contain);

  contain_destroy(contain);
  ellipse_major_destroy(major);
  ellipse_mt_destroy(mt);
  domain_destroy(dom);
}

static void assert_contain_zero(contain_t *contain, double x, double y)
{
  vec_t v, v0 = {0};

  CU_ASSERT_EQUAL(contain_eval(contain, x, y, &v), 0);
  CU_ASSERT_VEC_EQUAL(v, v0, 1e-8);
}

static void assert_contain_right(contain_t *contain, double x, double y)
{
  vec_t v;
  CU_ASSERT_EQUAL(contain_eval(contain, x, y, &v), 0);
  CU_ASSERT(v.x > 0);
}

static void assert_contain_left(contain_t *contain, double x, double y)
{
  vec_t v;
  CU_ASSERT_EQUAL(contain_eval(contain, x, y, &v), 0);
  CU_ASSERT(v.x < 0);
}

static void assert_contain_up(contain_t *contain, double x, double y)
{
  vec_t v;
  CU_ASSERT_EQUAL(contain_eval(contain, x, y, &v), 0);
  CU_ASSERT(v.y > 0);
}

static void assert_contain_down(contain_t *contain, double x, double y)
{
  vec_t v;
  CU_ASSERT_EQUAL(contain_eval(contain, x, y, &v), 0);
  CU_ASSERT(v.y < 0);
}

#define CU_ASSERT_CONTAIN_ZERO(C, x, y) assert_contain_zero(C, x, y)
#define CU_ASSERT_CONTAIN_RIGHT(C, x, y) assert_contain_right(C, x, y)
#define CU_ASSERT_CONTAIN_LEFT(C, x, y) assert_contain_left(C, x, y)
#define CU_ASSERT_CONTAIN_UP(C, x, y) assert_contain_up(C, x, y)
#define CU_ASSERT_CONTAIN_DOWN(C, x, y) assert_contain_down(C, x, y)

void test_contain_eval(void)
{
  domain_t *dom = factory_domain_square();
  ellipse_mt_t *mt = ellipse_mt_build();
  ellipse_major_t *major = ellipse_major_build();

  double max_length = 10;
  contain_t *contain = contain_new(dom, mt, major, max_length);
  CU_ASSERT_PTR_NOT_NULL_FATAL(contain);

  /*
    the centre of the domain should evaluate to zero, we snudge
    in a bit to account for the coarse mesh on which the metric
    tensor is defined
  */

  CU_ASSERT_CONTAIN_ZERO(contain, 5.5,  2.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 5.5, 30.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 5.5, 57.5);

  CU_ASSERT_CONTAIN_ZERO(contain, 30.0,  2.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 30.0, 30.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 30.0, 57.5);

  CU_ASSERT_CONTAIN_ZERO(contain, 54.5,  2.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 54.5, 30.5);
  CU_ASSERT_CONTAIN_ZERO(contain, 54.5, 57.5);

  /*
    close to the domain the containment field should repulse,
    we make weak assertions here, "rightward" is x-component
    positive etc -- we test just insisde the repulsion region
    which is 5 horizontally, 5/3 vertically
  */

  CU_ASSERT_CONTAIN_RIGHT(contain, 4.5,  6.5);
  CU_ASSERT_CONTAIN_RIGHT(contain, 4.5, 30.0);
  CU_ASSERT_CONTAIN_RIGHT(contain, 4.5, 53.5);

  CU_ASSERT_CONTAIN_LEFT(contain, 55.5, 6.5);
  CU_ASSERT_CONTAIN_LEFT(contain, 55.5, 30.0);
  CU_ASSERT_CONTAIN_LEFT(contain, 55.5, 53.5);

  CU_ASSERT_CONTAIN_UP(contain,  6.5, 1.5);
  CU_ASSERT_CONTAIN_UP(contain, 30.0, 1.5);
  CU_ASSERT_CONTAIN_UP(contain, 53.5, 1.5);

  CU_ASSERT_CONTAIN_DOWN(contain,  6.5, 58.5);
  CU_ASSERT_CONTAIN_DOWN(contain, 30.0, 58.5);
  CU_ASSERT_CONTAIN_DOWN(contain, 53.5, 58.5);

  /* corners */

  CU_ASSERT_CONTAIN_RIGHT(contain, 4.5,  1.5);
  CU_ASSERT_CONTAIN_UP(contain, 4.5,  1.5);

  CU_ASSERT_CONTAIN_LEFT(contain, 55.5,  1.5);
  CU_ASSERT_CONTAIN_UP(contain, 55.5,  1.5);

  CU_ASSERT_CONTAIN_RIGHT(contain, 4.5,  58.5);
  CU_ASSERT_CONTAIN_DOWN(contain, 4.5,  58.5);

  CU_ASSERT_CONTAIN_LEFT(contain, 55.5,  58.5);
  CU_ASSERT_CONTAIN_DOWN(contain, 55.5,  58.5);

#if 0

  FILE *fd = fopen("contain.csv", "w");

  for (size_t i = 0 ; i < 200 ; i++)
    {
      double x = 0.1 + i * 59.8 / 200;
      for (size_t j = 0 ; j < 200 ; j++)
        {
          double y = 0.1 + j * 59.8 / 200;
          vec_t v;

          contain_eval(contain, x, y, &v);
          fprintf(fd, "%f %f %f %f\n", x, y, v.x, v.y);
        }
    }

  fclose(fd);

#endif

  contain_destroy(contain);
  ellipse_major_destroy(major);
  ellipse_mt_destroy(mt);
  domain_destroy(dom);
}

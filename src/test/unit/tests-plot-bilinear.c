#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

// #define DOMAIN_WRITE_FIXTURES

#include <stdlib.h>

#include <unistd.h>

#include <plot/bilinear.h>
#include <plot/domain-geojson.h>

#include "fixture.h"
#include "assert-domain.h"
#include "assert-nan.h"
#include "tests-plot-bilinear.h"

CU_TestInfo tests_bilinear[] = {
  {"new", test_bilinear_new},
  {"xy-get", test_bilinear_xy_get},
  {"z-get/set", test_bilinear_z_get_set},
  {"CSV write", test_bilinear_csv_write},
  {"eval, quadratic", test_bilinear_eval_quadratic},
  {"eval, no data", test_bilinear_eval_nodata},
  {"curvature, linear", test_bilinear_curvature_linear},
  {"curvature, circular", test_bilinear_curvature_circular},
  {"integrate, interior", test_bilinear_integrate_interior},
  {"integrate, intersect", test_bilinear_integrate_intersect},

#ifdef WITH_JSON

  {"domain, node, 01", test_bilinear_node_domain_01},
  {"domain, node, 02", test_bilinear_node_domain_02},
  {"domain, node, 03", test_bilinear_node_domain_03},
  {"domain, pixel, 01", test_bilinear_pixel_domain_01},
  {"domain, pixel, 02", test_bilinear_pixel_domain_02},
  {"domain, pixel, 03", test_bilinear_pixel_domain_03},

#endif

  CU_TEST_INFO_NULL,
};

void test_bilinear_new(void)
{
  bilinear_t *B = bilinear_new(2, 2, 2);
  CU_ASSERT_PTR_NOT_NULL(B);
  bilinear_destroy(B);
}

void test_bilinear_xy_get(void)
{
  bilinear_t *B = bilinear_new(3, 3, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {1, 2}, {1, 2} };
  bilinear_bbox_set(B, bbox);

  double xy[2];

  bilinear_xy_get(B, 0, 0, xy);
  CU_ASSERT_DOUBLE_EQUAL(xy[0], 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(xy[1], 1, 1e-10);

  bilinear_xy_get(B, 1, 1, xy);
  CU_ASSERT_DOUBLE_EQUAL(xy[0], 1.5, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(xy[1], 1.5, 1e-10);

  bilinear_xy_get(B, 2, 2, xy);
  CU_ASSERT_DOUBLE_EQUAL(xy[0], 2, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(xy[1], 2, 1e-10);

  bilinear_destroy(B);
}

void test_bilinear_z_get_set(void)
{
  bilinear_t *B = bilinear_new(3, 3, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {1, 2}, {1, 2} };
  bilinear_bbox_set(B, bbox);

  const double z_src[2] = {1.5, 2.5};
  double z_dest[2];

  for (size_t i = 0 ; i < 3 ; i++)
    {
      for (size_t j = 0 ; j < 3 ; j++)
        {
          bilinear_z_set(B, i, j, z_src);
          bilinear_z_get(B, i, j, z_dest);
          for (size_t k = 0 ; k < 2 ; k++)
            CU_ASSERT_DOUBLE_EQUAL(z_src[k], z_dest[k], 1e-10);
        }
    }

  bilinear_destroy(B);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int assign_ij(bilinear_t *B, size_t i, size_t j, void *arg)
{
  double z[2] = { i, j };
  bilinear_z_set(B, i, j, z);
  return 0;
}

#pragma GCC diagnostic pop

void test_bilinear_csv_write(void)
{
  bilinear_t *B = bilinear_new(3, 3, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {1, 2}, {1, 2} };
  bilinear_bbox_set(B, bbox);
  bilinear_each(B, assign_ij, NULL);

  char filename[] = "/tmp/bilinear-XXXXXX";
  int fd = mkstemp(filename);
  CU_ASSERT_NOT_EQUAL_FATAL(fd, -1);

  FILE *stream = fdopen(fd, "w");
  CU_ASSERT_PTR_NOT_NULL_FATAL(stream);

  CU_ASSERT_EQUAL(bilinear_csv_write(B, stream), 0);
  fclose(stream);

  CU_ASSERT_EQUAL(access(filename, F_OK), 0);
  CU_ASSERT_EQUAL(unlink(filename), 0);

  bilinear_destroy(B);
}

/* port of corresponding test from bilinear.c */

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static double quadratic(double x, double y)
{
  return x * x + y * y;
}

static int assign_quadratic(bilinear_t *B, size_t i, size_t j, void *arg)
{
  double xy[2], z;
  bilinear_xy_get(B, i, j, xy);
  z = quadratic(xy[0], xy[1]);
  bilinear_z_set(B, i, j, &z);
  return 0;
}

#pragma GCC diagnostic pop

void test_bilinear_eval_quadratic(void)
{
  size_t nx = 50, ny = 50;
  bilinear_t *B = bilinear_new(nx, ny, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {0, 2}, {0, 2} };
  bilinear_bbox_set(B, bbox);

  bilinear_each(B, assign_quadratic, NULL);

  double eps = 1e-3;
  struct { double x, y; } S[] =
    {
      {eps, eps},
      {eps, 2.0 - eps},
      {2.0 - eps, eps},
      {2.0 - eps, 2.0 - eps},

      {0.1, 0.1},
      {0.2, 0.2},
      {0.3, 0.3},
      {1.0, 1.0},

      {0.0, 1.0},
      {0.3, 1.0},
      {0.6, 1.0},
      {0.9, 1.0},
    };
  size_t n = sizeof(S) / sizeof(S[0]);

  for (size_t i = 0 ; i < n ; i++)
    {
      double z0 = quadratic(S[i].x, S[i].y), z1;
      CU_ASSERT(bilinear_eval(B, S[i].x, S[i].y, &z1) == 0);
      CU_ASSERT_DOUBLE_EQUAL(z0, z1, eps);
    }

  bilinear_destroy(B);
}

/*
  check nodata - a 2x2 cell grid with the centre point as nodata. The
  interpolant is defined on the square less its circumscribed diamond,
  and we check that the data/nodata status is right.
*/

void test_bilinear_eval_nodata(void)
{
  size_t nx = 3, ny = 3;
  bilinear_t *B = bilinear_new(nx, ny, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {0, 2}, {0, 2} };
  bilinear_bbox_set(B, bbox);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          double xy[2];
          bilinear_xy_get(B, i, j, xy);

          double x = xy[0], y = xy[1], z;

          if ((x - 1) * (x - 1) + (y - 1) * (y - 1) < 0.1)
            z = NAN;
          else
            z = x * x + y * y;

          bilinear_z_set(B, i, j, &z);
        }
    }

  double z;

  CU_ASSERT_EQUAL(bilinear_eval(B, 0.1, 0.1, &z), 0);
  CU_ASSERT_NOT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 0.1, 1.9, &z), 0);
  CU_ASSERT_NOT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 1.9, 0.1, &z), 0);
  CU_ASSERT_NOT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 1.9, 1.9, &z), 0);
  CU_ASSERT_NOT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 0.9, 0.9, &z), 0);
  CU_ASSERT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 0.9, 1.1, &z), 0);
  CU_ASSERT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 1.1, 0.9, &z), 0);
  CU_ASSERT_NAN(z);

  CU_ASSERT_EQUAL(bilinear_eval(B, 1.1, 1.1, &z), 0);
  CU_ASSERT_NAN(z);

  bilinear_destroy(B);
}

void test_bilinear_curvature_linear(void)
{
  size_t nx = 3, ny = 3;
  bilinear_t *B = bilinear_new(nx, ny, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {0, 1}, {0, 1} };
  bilinear_bbox_set(B, bbox);

  const double z[] = { 1, 1 };

  for (size_t i = 0 ; i < nx ; i++)
    for (size_t j = 0 ; j < ny ; j++)
      bilinear_z_set(B, i, j, z);

  bilinear_t *Bc = bilinear_curvature(B);

  CU_ASSERT_PTR_NOT_NULL_FATAL(Bc);

  double k;

  bilinear_z_get(Bc, 1, 1, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 0, 0, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 0, 1, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 0, 2, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 1, 0, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 1, 2, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 2, 0, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 2, 1, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_z_get(Bc, 2, 2, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, 0, 1e-10);

  bilinear_destroy(Bc);
  bilinear_destroy(B);
}

void test_bilinear_curvature_circular(void)
{
  size_t nx = 3, ny = 3;
  bilinear_t *B = bilinear_new(nx, ny, 2);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = { {0, 1}, {0, 1} };
  bilinear_bbox_set(B, bbox);

  double
    z0[] = { 1 , 1 },
    z1[] = { sqrt(2), 0 },
    z2[] = { 1 , -1 };

  for (size_t j = 0 ; j < ny ; j++)
    {
      bilinear_z_set(B, 0, j, z0);
      bilinear_z_set(B, 1, j, z1);
      bilinear_z_set(B, 2, j, z2);
    }

  bilinear_t *Bc = bilinear_curvature(B);

  CU_ASSERT_PTR_NOT_NULL_FATAL(Bc);

  double k;

  bilinear_z_get(Bc, 1, 1, &k);
  CU_ASSERT_DOUBLE_EQUAL(k, sqrt(2), 1e-10);

  bilinear_destroy(Bc);
  bilinear_destroy(B);
}

void test_bilinear_integrate_interior(void)
{
  size_t nx = 256, ny = 256;
  bilinear_t *B = bilinear_new(nx, ny, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0.8, 2.2}, {0.8, 2.2}};
  bilinear_bbox_set(B, bbox);

  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          double xy[2];
          bilinear_xy_get(B, i, j, xy);
          double z = xy[0] + xy[1];
          bilinear_z_set(B, i, j, &z);
        }
    }

  vec_t v[] = { {1, 1}, {1, 2}, {2, 2}, {2, 1} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };

  CU_ASSERT_EQUAL(domain_insert(dom, &p), 0);
  CU_ASSERT_DOUBLE_EQUAL(domain_area(dom), 1, 1e-10);

  double I;
  int err = bilinear_integrate(B, dom, &I);

  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_DOUBLE_EQUAL(I, 3, 0.03);

  domain_destroy(dom);
  bilinear_destroy(B);
}

/*
  now the domain has a non-trivial intersection with the domain of
  definition of the grid: the former is [0, 2] x [0, 2], the latter
  is [1, 3] x [1, 3], so the intersection is [1, 2] x [1, 2], and
  the integral (as with case above) is 3.
*/

void test_bilinear_integrate_intersect(void)
{
  size_t nx = 256, ny = 256;
  bilinear_t *B = bilinear_new(nx, ny, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0, 2}, {0, 2}};
  bilinear_bbox_set(B, bbox);

  domain_t *dom = domain_new();
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          double xy[2];
          bilinear_xy_get(B, i, j, xy);
          double z = xy[0] + xy[1];
          bilinear_z_set(B, i, j, &z);
        }
    }

  vec_t v[] = { {1, 1}, {1, 3}, {3, 3}, {3, 1} };
  size_t n = sizeof(v) / sizeof(vec_t);
  polygon_t p = { .n = n, .v = v };

  CU_ASSERT_EQUAL(domain_insert(dom, &p), 0);
  CU_ASSERT_DOUBLE_EQUAL(domain_area(dom), 4, 1e-10);

  double I;
  int err = bilinear_integrate(B, dom, &I);

  CU_ASSERT_EQUAL(err, 0);
  CU_ASSERT_DOUBLE_EQUAL(I, 3, 0.02);

  domain_destroy(dom);
  bilinear_destroy(B);
}

/*
  compare domain output with that in fixtures, those should
  be inspected manually for correctness.
*/

void test_bilinear_node_domain_01(void)
{
  bilinear_t *B = bilinear_new(3, 3, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0, 2}, {0, 2}};
  bilinear_bbox_set(B, bbox);

  for (size_t i = 0 ; i < 3 ; i++)
    for (size_t j = 0 ; j < 3 ; j++)
      {
        double z = i + j;
        bilinear_z_set(B, i, j, &z);
      }

  const char *fix_path = fixture("geojson/unit-node-01.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_node_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);
  domain_destroy(dom);
  bilinear_destroy(B);
}

/*
   * * * *
   *   * *
   * *   *
   * * * *
*/

void test_bilinear_node_domain_02(void)
{
  bilinear_t *B = bilinear_new(4, 4, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0, 3}, {0, 3}};
  bilinear_bbox_set(B, bbox);

  double dat[4][4] =
    {
     {1,   1,   1, 1},
     {1,   1, NAN, 1},
     {1, NAN,   1, 1},
     {1,   1,   1, 1}
    };

  for (size_t i = 0 ; i < 4 ; i++)
    for (size_t j = 0 ; j < 4 ; j++)
      bilinear_z_set(B, i, j, &(dat[i][j]));

  const char *fix_path = fixture("geojson/unit-node-02.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_node_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);
  domain_destroy(dom);
  bilinear_destroy(B);
}

/*
  This is a regression test for a bug in 1.0.8 discovered in the rsmas
  example, a width one finger of data is removed by colinearity leaving
  a duplicate at the knuckle of the finger (the point (1, 1) here)

  * *
  * * * *
  * *
*/

void test_bilinear_node_domain_03(void)
{
  bilinear_t *B = bilinear_new(4, 3, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0, 3}, {0, 2}};
  bilinear_bbox_set(B, bbox);

  double dat1[4][3] =
    {
     {  1, 1,   1},
     {  1, 1,   1},
     {NAN, 1, NAN},
     {NAN, 1, NAN}
    };

  for (size_t i = 0 ; i < 4 ; i++)
    for (size_t j = 0 ; j < 3 ; j++)
      bilinear_z_set(B, i, j, &dat1[i][j]);

  const char *fix_path = fixture("geojson/unit-node-03.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_node_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);
  domain_destroy(dom);
  bilinear_destroy(B);
}

/*
  pixel versions of above -- note that the bounding boxes
  here are of the (intrinsically node-aligned) bilinear
  grid, so a half-pixel smaller than the resulting domain,
  this infelicity will go away when we have implement RBF
  interpolation
*/

void test_bilinear_pixel_domain_01(void)
{
  bilinear_t *B = bilinear_new(3, 3, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0.5, 2.5}, {0.5, 2.5}};
  bilinear_bbox_set(B, bbox);

  for (size_t i = 0 ; i < 3 ; i++)
    for (size_t j = 0 ; j < 3 ; j++)
      {
        double z = i + j + 1;
        bilinear_z_set(B, i, j, &z);
      }

  const char *fix_path = fixture("geojson/unit-pixel-01.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_pixel_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);
  domain_destroy(dom);
  bilinear_destroy(B);
}

void test_bilinear_pixel_domain_02(void)
{
  bilinear_t *B = bilinear_new(4, 4, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0.5, 3.5}, {0.5, 3.5}};
  bilinear_bbox_set(B, bbox);

  double dat[4][4] =
    {
     {1,   1,   1, 1},
     {1,   1, NAN, 1},
     {1, NAN,   1, 1},
     {1,   1,   1, 1}
    };

  for (size_t i = 0 ; i < 4 ; i++)
    for (size_t j = 0 ; j < 4 ; j++)
      bilinear_z_set(B, i, j, &(dat[i][j]));

  const char *fix_path = fixture("geojson/unit-pixel-02.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_pixel_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);

  domain_destroy(dom);
  bilinear_destroy(B);
}

void test_bilinear_pixel_domain_03(void)
{
  bilinear_t *B = bilinear_new(4, 3, 1);
  CU_ASSERT_PTR_NOT_NULL_FATAL(B);

  bbox_t bbox = {{0.5, 3.5}, {0.5, 2.5}};
  bilinear_bbox_set(B, bbox);

  double dat1[4][3] =
    {
     {  1, 1,   1},
     {  1, 1,   1},
     {NAN, 1, NAN},
     {NAN, 1, NAN}
    };

  for (size_t i = 0 ; i < 4 ; i++)
    for (size_t j = 0 ; j < 3 ; j++)
      bilinear_z_set(B, i, j, &dat1[i][j]);

  const char* fix_path = fixture("geojson/unit-pixel-03.geojson");

  CU_ASSERT_PTR_NOT_NULL_FATAL(fix_path);

  domain_t
    *dom = bilinear_pixel_domain(B),
    *dom_fix = domain_read(fix_path);

  CU_ASSERT_PTR_NOT_NULL(dom);
  CU_ASSERT_PTR_NOT_NULL_FATAL(dom_fix);

#ifdef DOMAIN_WRITE_FIXTURES
  domain_write(fix_path, dom);
#endif

  CU_ASSERT_DOMAIN_EQUAL(dom, dom_fix);

  domain_destroy(dom_fix);

  domain_destroy(dom);
  bilinear_destroy(B);
}

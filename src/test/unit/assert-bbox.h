/*
  assert_bbox.h
  Copyright (c) J.J. Green 2015
*/

#ifndef ASSERT_BBOX_H
#define ASSERT_BBOX_H

#include <plot/bbox.h>

#define CU_ASSERT_BBOX_EQUAL(A, B, eps) assert_bbox_equal(A, B, eps)

void assert_bbox_equal(bbox_t, bbox_t, double);

#endif

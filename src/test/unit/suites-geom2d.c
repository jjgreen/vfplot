#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <CUnit/CUnit.h>

#include "tests-geom2d-contact.h"
#include "tests-geom2d-quadratic.h"
#include "tests-geom2d-ellipse.h"
#include "tests-geom2d-mat.h"
#include "tests-geom2d-sdcontact.h"
#include "tests-geom2d-vec.h"

#include "cunit-compat.h"

#define ENTRY(a, b) CU_Suite_Entry((a), NULL, NULL, (b))

static CU_SuiteInfo suites[] =
  {
    ENTRY("contact", tests_contact),
    ENTRY("ellipse", tests_ellipse),
    ENTRY("matrix", tests_matrix),
    ENTRY("quadratic", tests_quadratic),
    ENTRY("semi-degenerate contact", tests_sdcontact),
    ENTRY("vector", tests_vector),
    CU_SUITE_INFO_NULL,
  };

void suites_load(void)
{
  assert(NULL != CU_get_registry());
  assert(!CU_is_test_running());

  if (CU_register_suites(suites) != CUE_SUCCESS)
    {
      fprintf(stderr,
              "geom2d suite registration failed - %s\n",
	      CU_get_error_msg());
      exit(EXIT_FAILURE);
    }
}

/*
  assert_matric.c
  J.J. Green 2015
*/

#include <CUnit/CUnit.h>
#include "assert-mat.h"

void assert_mat_equal(mat_t A, mat_t B, double eps)
{
  CU_ASSERT_DOUBLE_EQUAL(A.a, B.a, eps);
  CU_ASSERT_DOUBLE_EQUAL(A.b, B.b, eps);
  CU_ASSERT_DOUBLE_EQUAL(A.c, B.c, eps);
  CU_ASSERT_DOUBLE_EQUAL(A.d, B.d, eps);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/svg-colours.h>

#include "tests-plot-svg-colours.h"

CU_TestInfo tests_svg_colours[] = {
  {"present", test_svg_colours_present},
  {"absent", test_svg_colours_absent},
  CU_TEST_INFO_NULL,
};

void test_svg_colours_present(void)
{
  const struct svg_colour_t *colour = svg_colour("sienna");
  CU_ASSERT_PTR_NOT_NULL_FATAL(colour);
  CU_ASSERT_EQUAL(colour->r, 160);
  CU_ASSERT_EQUAL(colour->g, 82);
  CU_ASSERT_EQUAL(colour->b, 45);
  CU_ASSERT_EQUAL(strcmp(colour->name, "sienna"), 0);
}

void test_svg_colours_absent(void)
{
  const struct svg_colour_t *colour = svg_colour("nosuch");
  CU_ASSERT_PTR_NULL(colour);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <plot/scan-fill.h>
#include <plot/error.h>

#include "tests-plot-scan-fill.h"

CU_TestInfo tests_scan_fill[] = {
  {"empty", test_scan_fill_empty},
  {"named", test_scan_fill_named},
  CU_TEST_INFO_NULL
};

void test_scan_fill_empty(void)
{
  fill_t fill;

  CU_ASSERT_NOT_EQUAL(scan_fill("", &fill), ERROR_OK);
}

void test_scan_fill_named(void)
{
  fill_t fill;

  CU_ASSERT_EQUAL(scan_fill("red", &fill), ERROR_OK);
  CU_ASSERT_EQUAL(fill.type, fill_colour);
  CU_ASSERT_EQUAL(fill.colour.type, colour_rgb);
  CU_ASSERT_EQUAL(fill.colour.rgb.r, 255);
  CU_ASSERT_EQUAL(fill.colour.rgb.g, 0);
  CU_ASSERT_EQUAL(fill.colour.rgb.b, 0);
}

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_scan_length[];

void test_scan_length_empty(void);
void test_scan_length_no_unit(void);
void test_scan_length_P(void);
void test_scan_length_p(void);
void test_scan_length_i(void);
void test_scan_length_m(void);
void test_scan_length_c(void);

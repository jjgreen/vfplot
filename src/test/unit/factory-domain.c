#include <CUnit/CUnit.h>
#include "factory-domain.h"

/*
  domains depend on JSON (so libjannson) support, and that's
  optional; so in test we use factories father than fixtures,
  that means we can run tests without JSON support.
*/

domain_t* factory_domain_square(void)
{
  vec_t v[4] = { {0, 0}, {0, 60}, {60, 60}, {60, 0} };
  polygon_t p = { .n = 4, .v = v };
  domain_t *dom = domain_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);
  CU_ASSERT_EQUAL(domain_insert(dom, &p), 0);

  return dom;
}

domain_t* factory_domain_nested(void)
{
  vec_t
    v0[4] =
    {
     {0,  0},
     {0,  60},
     {60, 60},
     {60, 0}
    },
    v1[4] =
    {
     {10, 10},
     {10, 20},
     {20, 20},
     {20, 10}
    },
    v2[4] =
    {
     {30, 30},
     {30, 50},
     {50, 50},
     {50, 30}
    },
    v3[4] =
    {
     {37, 37},
     {37, 43},
     {43, 43},
     {43, 37}
    };
  polygon_t p[4] =
    {
     { .n = 4, .v = v0 },
     { .n = 4, .v = v1 },
     { .n = 4, .v = v2 },
     { .n = 4, .v = v3 }
    };

  domain_t *dom = domain_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  for (size_t i = 0 ; i < 4 ; i++)
    CU_ASSERT_EQUAL(domain_insert(dom, p + i), 0);

  return dom;
}

domain_t* factory_domain_components(void)
{
  vec_t
    v0[4] =
    {
     {0,  0},
     {0,  20},
     {60, 20},
     {60, 0}
    },
    v1[4] =
    {
     {0,  40},
     {0,  60},
     {60, 60},
     {60, 40}
    };
  polygon_t p[2] =
    {
     { .n = 4, .v = v0 },
     { .n = 4, .v = v1 }
    };

  domain_t *dom = domain_new();

  CU_ASSERT_PTR_NOT_NULL_FATAL(dom);

  for (size_t i = 0 ; i < 2 ; i++)
    CU_ASSERT_EQUAL(domain_insert(dom, p + i), 0);

  return dom;
}

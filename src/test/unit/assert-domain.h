/*
  assert-domain.h
  Copyright (c) J.J. Green 2023
*/

#ifndef ASSERT_DOMAIN_H
#define ASSERT_DOMAIN_H

#include <plot/domain.h>
#include <stddef.h>

#define CU_ASSERT_DOMAIN_EQUAL(D1, D2) assert_domain_equal(D1, D2, 1024)

void assert_domain_equal(const domain_t*, const domain_t*, size_t);

#endif

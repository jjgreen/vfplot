#include <CUnit/CUnit.h>

extern CU_TestInfo tests_scan_colour[];

void test_scan_colour_empty(void);
void test_scan_colour_grey(void);
void test_scan_colour_rgb(void);
void test_scan_colour_named(void);

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_quadratic[];

void test_quadratic_roots_complex_1(void);
void test_quadratic_roots_complex_2(void);
void test_quadratic_roots_complex_3(void);
void test_quadratic_roots_complex_4(void);
void test_quadratic_roots_complex_5(void);
void test_quadratic_roots_complex_6(void);

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_matrix[];

void test_matrix_rotate(void);
void test_matrix_add(void);
void test_matrix_subtract(void);
void test_matrix_transpose(void);
void test_matrix_determinant(void);
void test_matrix_inverse(void);
void test_matrix_adjoint(void);
void test_matrix_trace(void);
void test_matrix_scalar_multiply(void);
void test_matrix_vector_multiply(void);
void test_matrix_matrix_multiply(void);
void test_matrix_resolvant(void);
void test_matrix_qform(void);
void test_matrix_dqform(void);
void test_matrix_pdir(void);
void test_matrix_xmax(void);
void test_matrix_ymax(void);

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <geom2d/mat.h>
#include "assert-mat.h"
#include "assert-vec.h"
#include "tests-geom2d-mat.h"

CU_TestInfo tests_matrix[] = {
  {"rotation", test_matrix_rotate},
  {"add", test_matrix_add},
  {"subtract", test_matrix_subtract},
  {"transpose", test_matrix_transpose},
  {"determinant", test_matrix_determinant},
  {"inverse", test_matrix_inverse},
  {"adjoint", test_matrix_adjoint},
  {"trace", test_matrix_trace},
  {"scalar multiply", test_matrix_scalar_multiply},
  {"vector multiply", test_matrix_vector_multiply},
  {"matrix multiply", test_matrix_matrix_multiply},
  {"resolvant", test_matrix_resolvant},
  {"quadratic form", test_matrix_qform},
  {"derivative of quadratic form", test_matrix_dqform},
  {"principal direction", test_matrix_pdir},
  {"x-max", test_matrix_xmax},
  {"y-max", test_matrix_ymax},
  CU_TEST_INFO_NULL
};

void test_matrix_rotate(void)
{
  mat_t
    A = mrot(M_PI/2),
    B = {0, -1, 1, 0};

  CU_ASSERT_MAT_EQUAL(A, B, 1e-10);
}

void test_matrix_add(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = {4, 3, 2, 1},
    C = madd(A, B),
    D = {5, 5, 5, 5};

  CU_ASSERT_MAT_EQUAL(C, D, 1e-10);
}

void test_matrix_subtract(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = {4, 3, 2, 1},
    C = msub(A, B),
    D = {-3, -1, 1, 3};

  CU_ASSERT_MAT_EQUAL(C, D, 1e-10);
}

void test_matrix_transpose(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = mtranspose(A),
    C = {1, 3, 2, 4};

  CU_ASSERT_MAT_EQUAL(B, C, 1e-10);
}

void test_matrix_determinant(void)
{
  mat_t A = {1, 2, 3, 4};
  double d = mdet(A);

  CU_ASSERT_DOUBLE_EQUAL(d, -2, 1e-10);
}

void test_matrix_inverse(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = minv(A),
    C = {-2.0, 1.0, 1.5, -0.5};

  CU_ASSERT_MAT_EQUAL(B, C, 1e-10);
}

void test_matrix_adjoint(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = madj(A),
    C = {4, -2, -3, 1};

  CU_ASSERT_MAT_EQUAL(B, C, 1e-10);
}

void test_matrix_trace(void)
{
  mat_t A = {1, 2, 3, 4};

  CU_ASSERT_DOUBLE_EQUAL(mtrace(A), 5, 1e-10);
}

void test_matrix_scalar_multiply(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = msmul(2, A),
    C = {2, 4, 6, 8};

  CU_ASSERT_MAT_EQUAL(B, C, 1e-10);
}

void test_matrix_vector_multiply(void)
{
  mat_t A = {1, 2, 3, 4};
  vec_t
    u = {1, 2},
    v = mvmul(A, u),
    w = {5, 11};

  CU_ASSERT_VEC_EQUAL(v, w, 1e-10);
}

void test_matrix_matrix_multiply(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = {4, 3, 2, 1},
    C = mmmul(A, B),
    D = {8, 5, 20, 13};

  CU_ASSERT_MAT_EQUAL(C, D, 1e-10);
}

void test_matrix_resolvant(void)
{
  mat_t
    A = {1, 2, 3, 4},
    B = mres(A, 2),
    C = {-1, 2, 3, 2};

  CU_ASSERT_MAT_EQUAL(B, C, 1e-10);
}

void test_matrix_qform(void)
{
  vec_t v = {1, 2};
  mat_t A = {1, 2, 3, 4};
  double w = mqform(A, v);

  CU_ASSERT_DOUBLE_EQUAL(w, 27, 1e-10);
}

void test_matrix_dqform(void)
{
  vec_t v = {1, 2};
  mat_t A = {1, 2, 3, 4};
  vec_t
    w = mdqform(A, v),
    wexp = {12, 21};

  CU_ASSERT_VEC_EQUAL(w, wexp, 1e-10);
}

void test_matrix_pdir(void)
{
  vec_t v = {-1, 1};
  mat_t A = {0, 1, 0, 1};
  vec_t
    w = mpdir(A, v),
    wexp = {1/sqrt(2), 1/sqrt(2)};

  CU_ASSERT_VEC_EQUAL(w, wexp, 1e-10);
}

void test_matrix_xmax(void)
{
  mat_t
    A = {1, 0, 0, 1},
    B = {0, 1, -1, 0},
    C = {0, 1, 0, 0},
    D = {0, 0, 1, 0};

  CU_ASSERT_DOUBLE_EQUAL(mxmax(A), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mxmax(B), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mxmax(C), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mxmax(D), 0, 1e-10);
}

void test_matrix_ymax(void)
{
  mat_t
    A = {1, 0, 0, 1},
    B = {0, 1, -1, 0},
    C = {0, 1, 0, 0},
    D = {0, 0, 1, 0};

  CU_ASSERT_DOUBLE_EQUAL(mymax(A), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mymax(B), 1, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mymax(C), 0, 1e-10);
  CU_ASSERT_DOUBLE_EQUAL(mymax(D), 1, 1e-10);
}

/*
  assert_regex.h
  Copyright (c) J.J. Green 2020
*/

#ifndef ASSERT_REGEX_H
#define ASSERT_REGEX_H

#define CU_ASSERT_REGEX(regex, text) assert_regex(regex, text)

void assert_regex(const char*, const char*);

#endif

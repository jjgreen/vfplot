/*
  assert_polygon.h
  Copyright (c) J.J. Green 2015
*/

#ifndef ASSERT_POLYGON_H
#define ASSERT_POLYGON_H

#include <plot/polygon.h>

#define CU_ASSERT_POLYGON_EQUAL(P, Q, eps) assert_polygon_equal(P, Q, eps)

void assert_polygon_equal(polygon_t, polygon_t, double);

#endif

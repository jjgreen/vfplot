/*
  assert_nan.h
  Copyright (c) J.J. Green 2022
*/

#ifndef ASSERT_NAN_H
#define ASSERT_NAN_H

#include <math.h>

#define CU_ASSERT_NAN(x) CU_ASSERT_NOT_EQUAL(isnan(x), 0)
#define CU_ASSERT_NOT_NAN(x) CU_ASSERT_EQUAL(isnan(x), 0)

#endif

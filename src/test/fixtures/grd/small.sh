#!/bin/sh

set -e

# Some (very) small fixtures for unit-tests against the
# grd2 readers, these are too small to create plots.
# The u node file has one NaN at (0, 0), the v file does
# not, that should still be a valid pair.  The pixel
# files are generated from the node

gmt xyz2grd -Gsmall-node-u.grd -I1 -R0/2/0/2 <<EOF
0, 0, 1
0, 1, 1
0, 2, 1
1, 0, 1
1, 1, 1
1, 2, 1
2, 0, 1
2, 1, 1
2, 2, 1
EOF

gmt xyz2grd -Gsmall-node-v.grd -I1 -R0/2/0/2 <<EOF
0, 0, 0
0, 1, 0
0, 2, 0
1, 0, 0
1, 1, 0
1, 2, 0
2, 0, 0
2, 1, 0
2, 2, 0
EOF

gmt grdsample -T -Gsmall-pixel-u.grd small-node-u.grd
gmt grdsample -T -Gsmall-pixel-v.grd small-node-v.grd

#!/usr/bin/env bats

load 'shared'

setup()
{
    program='vfplot'
    path="${src_dir}/vfplot/${program}"
    geometry="-m 4/4/0 -w 4i"
}

@test 'vfplot, --version' {
    run $path --version
    [ $status -eq 0 ]
    [ "${output}" = "${program} ${version}" ]
}

@test 'vfplot, --help' {
    $path --help
}

@test 'vfplot, --placement list' {
    $path --placement list
}

@test 'vfplot, --unknown' {
    run $path --unknown
    [ $status -ne 0 ]
}

@test 'vfplot, --placement hedgehog' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement hedgehog \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --placement adaptive' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement adaptive \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --placement unknown' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement unknown \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --number --scale (hedgehog)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement hedgehog \
        --number 100 \
        --scale 1 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --number --scale (adaptive)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement adaptive \
        --iterations 30/5 \
        --number 100 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --number, no --scale (adaptive)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement adaptive \
        --iterations 30/5 \
        --number 100 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --scale, no --number (adaptive)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --placement adaptive \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --margin too large, autoscale fails' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path \
        --iterations 30/5 \
        --width 4i \
        --number 100 \
        --margin 100/100 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop X (bad argument)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop X \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1x (bad unit)' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop 1x \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1c' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop 1c \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --crop 1c/0' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop 1c/0 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --crop 1c/0/1c' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop 1c/0/1c \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --crop 1c/1c/0/1c' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --crop 1c/1c/0/1c \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --domain' {
    skip_unless_with_json
    plot='circular'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    geojson="${fixture_dir}/geojson/nested-squares.geojson"
    run $path $geometry \
        --domain $geojson \
        --iterations 30/5 \
        --scale 0.736328 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --graphic-state' {
    skip_unless_with_json
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    state="${BATS_TEST_TMPDIR}/${plot}.json"
    run $path $geometry \
        --graphic-state $state \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${state}" ]
    rm -f $eps
    run $path $geometry \
        --graphic-state $state \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${state}" ]
}

@test 'vfplot, --dump-domain' {
    skip_unless_with_json
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    geojson="${BATS_TEST_TMPDIR}/${plot}.geojson"
    run $path $geometry \
        --dump-domain $geojson \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ -e "${geojson}" ]
    rm -f $eps
    run $path $geometry \
        --dump $geojson \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ -e "${eps}" ]
}

@test 'vfplot, --dump-vectors' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    csv="${BATS_TEST_TMPDIR}/${plot}.csv"
    run $path $geometry \
        --dump-vectors $csv \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${csv}" ]
}

@test 'vfplot, --histogram' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    hst="${BATS_TEST_TMPDIR}/${plot}.hst"
    run $path $geometry \
        --histogram $hst \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${hst}" ]
}

@test 'vfplot, --escaped' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    esc="${BATS_TEST_TMPDIR}/${plot}.csv"
    run $path $geometry \
        --escaped $esc \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${esc}" ]
}

@test 'vfplot, --symbol list' {
    $path --symbol list
}

@test 'vfplot, --symbol arrow' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --symbol arrow \
        --iterations 30/5 \
        --scale 1.550781 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --symbol triangle' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --symbol triangle \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --aspect positive' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --aspect 10 \
        --iterations 30/5 \
        --scale 1.402344 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --aspect zero' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --aspect 0 \
        --iterations 30/5 \
        --scale 1.402344 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --log-path' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    log="${BATS_TEST_TMPDIR}/${plot}.log"
    run $path $geometry \
        --log-path $log \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${log}" ]
}

@test 'vfplot, --log-path unwritable' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    log="${BATS_TEST_TMPDIR}/no/such/directory/${plot}.log"
    run $path $geometry \
        --log-path $log \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
    [ ! -e "${log}" ]
}

@test 'vfplot, --log-level valid' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    log="${BATS_TEST_TMPDIR}/${plot}.log"
    run $path $geometry \
        --log-path $log \
        --log-level DEBUG \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${log}" ]
}

@test 'vfplot, --log-level invalid' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    log="${BATS_TEST_TMPDIR}/${plot}.log"
    run $path $geometry \
        --log-path $log \
        --log-level INVALID \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
    [ ! -e "${log}" ]
}

# FIXME: make some assertions on the contents of $json, this
# will need jq and some wrapper functions ...

@test 'vfplot, --statistics' {
    skip_unless_with_json
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    json="${BATS_TEST_TMPDIR}/${plot}-statistics.json"
    run $path $geometry \
        --statistics $json \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    [ -e "${json}" ]
}

@test 'vfplot, --output-format list' {
    $path --output-format list
}

@test 'vfplot, --output-format eps' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --output-format eps \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --output-format povray' {
    plot='cylinder'
    pov="${BATS_TEST_TMPDIR}/${plot}.pov"
    run $path $geometry \
        --output-format povray \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $pov
    [ $status -eq 0 ]
    [ -e "${pov}" ]
}

@test 'vfplot, --output-format unknown' {
    plot='cylinder'
    moot="${BATS_TEST_TMPDIR}/${plot}.moot"
    run $path $geometry \
        --output-format unknown \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $moot
    [ $status -ne 0 ]
    [ ! -e "${moot}" ]
}

@test 'vfplot, --break list' {
    $path --break list
}

@test 'vfplot, --break grid' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --break grid \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --break super' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --break super \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    rm -f $eps
}

@test 'vfplot, --break midclean' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --break midclean \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --break postclean' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --break postclean \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --break none' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --break none \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format list' {
    $path --format list
}

@test 'vfplot, --format grd2, explicit' {
    skip_unless_with_netcdf
    plot='grd2-input'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    run $path $geometry \
        --format grd2  \
        --iterations 30/5 \
        --scale 1.402344 \
        --output $eps \
        $grdu $grdv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format grd2, implicit' {
    skip_unless_with_netcdf
    plot='grd2-input'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    run $path $geometry \
        --iterations 30/5 \
        --scale 1.402344 \
        --output $eps \
        $grdu $grdv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format grd2, too many files' {
    plot='grd2-input'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    grdu="${fixture_dir}/grd/cyl-u.grd"
    grdv="${fixture_dir}/grd/cyl-v.grd"
    grdw="${fixture_dir}/grd/cyl-v.grd"
    run $path $geometry \
        --iterations 30/5 \
        --scale 1.402344 \
        --output $eps \
        $grdu $grdv $grdw
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --format grd2, absent files' {
    plot='grd2-input'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    grdu="${fixture_dir}/grd/absent-u.grd"
    grdv="${fixture_dir}/grd/absent-v.grd"
    run $path $geometry \
        --format grd2  \
        --iterations 30/5 \
        --scale 1.402344 \
        --output $eps \
        $grdu $grdv
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

# realistic csv files are large, so rather than have a fixture
# we generate them on-the-fly

@test 'vfplot, --format csv, explicit' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    csv="${BATS_TEST_TMPDIR}/${plot}.csv"
    $path --dump-vectors $csv --break grid $geometry -t $plot
    [ -e "${csv}" ]
    run $path $geometry \
        --format csv  \
        --grid-size 128/128 \
        --grid-range 0/1/0/1 \
        --iterations 30/5 \
        --scale 0.222168 \
        --output $eps \
        $csv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format csv, implicit' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    csv="${BATS_TEST_TMPDIR}/${plot}.csv"
    $path --dump-vectors $csv --break grid $geometry -t $plot
    [ -e "${csv}" ]
    run $path $geometry \
        --grid-size 128/128 \
        --grid-range 0/1/0/1 \
        --iterations 30/5 \
        --scale 0.222168 \
        --output $eps \
        $csv
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format mat (v7), explict' {
    skip_unless_with_matlab
    plot='mat-v7-input-explicit'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    mat="${fixture_dir}/mat/shear-v7.mat"
    run $path $geometry \
        --format mat  \
        --iterations 30/5 \
        --scale 0.003525 \
        --output $eps \
        $mat
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format mat (v7), implicit' {
    skip_unless_with_matlab
    plot='mat-v7-input-implicit'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    mat="${fixture_dir}/mat/shear-v7.mat"
    run $path $geometry \
        --iterations 30/5 \
        --scale 0.003525 \
        --output $eps \
        $mat
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format gfs, explict' {
    skip_unless_with_gerris
    plot='gfs-input-explicit'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    gfs="${fixture_dir}/gfs/house-001.gfs"
    run $path -w 6i -m 4/4/0.5 \
        --format gfs \
        --iterations 30/5 \
        --scale 0.002663 \
        --output $eps \
        $gfs
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format gfs, implict' {
    skip_unless_with_gerris
    plot='gfs-input-implicit'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    gfs="${fixture_dir}/gfs/house-001.gfs"
    run $path -w 6i -m 4/4/0.5 \
        --iterations 30/5 \
        --scale 0.002663 \
        --output $eps \
        $gfs
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --format absent, 1 unknown file' {
    name='unknown-1'
    eps="${BATS_TEST_TMPDIR}/${name}.eps"
    input="${fixture_dir}/unknown/01.bin"
    run $path $geometry \
        --output $eps \
        $input
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --format absent, 2 unknown files' {
    name='unknown-2'
    eps="${BATS_TEST_TMPDIR}/${name}.eps"
    input1="${fixture_dir}/unknown/01.bin"
    input2="${fixture_dir}/unknown/02.bin"
    run $path $geometry \
        --output $eps \
        $input1 $input2
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --animate' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --animate \
        --iterations 5/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    for i in $(seq 0 4)
    do
        for j in $(seq 0 4)
        do
            eps=$(printf "anim.%04i.%04i.eps" $i $j)
            [ -e "${eps}" ]
            rm -f $eps
        done
    done
}

@test 'vfplot, --animate-dir' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    animate_dir="${BATS_TEST_TMPDIR}"
    run $path $geometry \
        --animate-dir $animate_dir \
        --iterations 5/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
    for i in $(seq 0 4)
    do
        for j in $(seq 0 4)
        do
            eps=$(printf "%s/anim.%04i.%04i.eps" $animate_dir $i $j)
            [ -e "${eps}" ]
        done
    done
}

@test 'vfplot, --fill grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --fill 100 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --fill RGB' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --fill 10/10/20 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --fill named-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --fill beige \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --fill bad-arguument' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --fill 1000/10 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --pen no-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --pen 2 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --pen grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --pen 2/125 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --pen colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --pen 2/red \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --domain-pen no-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --domain-pen 2 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --domain-pen grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --domain-pen 2/125 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --domain-pen colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --domain-pen 2/red \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --network-pen no-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --network-pen 2 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --network-pen grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --network-pen 2/125 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --network-pen colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --network-pen 2/red \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-pen no-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-pen 2 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-pen grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-pen 2/125 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-pen colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-pen 2/red \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-fill grey' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-fill 200 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-fill RGB' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-fill 1/2/3 \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --ellipse-fill named-colour' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --ellipse \
        --ellipse-fill teal \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --sort list' {
    $path --sort list
}

@test 'vfplot, --sort none' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --sort none \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --sort longest' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --sort longest \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --sort shortest' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --sort shortest \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --sort straightest' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --sort straightest \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --sort bendiest' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --sort bendiest \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --height' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path \
        --height 5i \
        --iterations 30/5 \
        --scale 0.291016 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --width' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path \
        --width 5i \
        --iterations 30/5 \
        --scale 0.291016 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --height --width' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path \
        --height 5i \
        --width 5i \
        --iterations 30/5 \
        --scale 0.291016 \
        --test $plot \
        --output $eps
    [ $status -ne 0 ]
    [ ! -e "${eps}" ]
}

@test 'vfplot, --iterations absent' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --iterations outer' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --iterations outer/inner' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --test list' {
    $path --test list
}

@test 'vfplot, --test circular' {
    plot='circular'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 0.607422 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --test cylinder' {
    plot='cylinder'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 1.914062 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --test electro2' {
    plot='electro2'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 4.328125 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --test electro3' {
    plot='electro3'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 4.515625 \
        --test $plot \
        --output $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

@test 'vfplot, --test smoska' {
    plot='smoska'
    eps="${BATS_TEST_TMPDIR}/${plot}.eps"
    run $path $geometry \
        --iterations 30/5 \
        --scale 5.046875 \
        --test $plot \
        --output $eps
    run $path -v -i 30/5 $geometry -t $plot -o $eps
    [ $status -eq 0 ]
    [ -e "${eps}" ]
}

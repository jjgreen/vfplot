Acceptance tests with Bats
==========================

These are the vfplot acceptance tests, and we use the *Bats* package
for those tests.  Bats seems to have had a chequered history, it was
originally developed by [Sam Stephenson][1] who decided to give-up on
it, it was forked to [bats-core][2] by the Bats community group, and
also by [Zoltán Tömböl][3] who added some more-advanced libraries for
[assertions][4].  The latter seems to have been abandoned, but have
been forked by the Bats community group, but not extensively developed.

The bats-core package is available as the [bats package][5] in Debian,
and so Ubuntu.  This is convenient for development, since GitLab CI
runs Ubuntu, although with the older 0.4.0 version of the package.
From Debian Buster Bullseye onward, version 1.2 will be available, and
that has some nice features like parallelisation of the tests.  There
do not seem to be any plans to add the assertions libraries to Debian.

So for the time-being, we use a local copy of the Bats library, it's
not large and is trivial to install/refresh from a GitHub release
tarball.


[1]: https://github.com/sstephenson
[2]: https://github.com/bats-core/bats-core
[3]: https://github.com/ztombol
[4]: https://github.com/ztombol/bats-assert
[5]: https://packages.debian.org/search?keywords=bats

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "geom2d/ellipse.h"
#include "geom2d/quadratic.h"
#include "geom2d/sincos.h"

#include <log.h>

#include <math.h>
#include <stdio.h>

/* the (inverse of the) metric tensor */

int mat_ellipse(const mat_t *pM, ellipse_t *E)
{
  mat_t M = *pM;
  double A[3] = { mdet(M), -M.a - M.d, 1.0 }, s[2];
  double complex r[2];

  if (quadratic_roots_complex(A, r) != 0)
    {
      log_error("improper metric tensor");
      return 1;
    }

  /*
    the roots can be properly complex, in this case we use
    the real part of that conjugate pair, but really should
    log that this happens ...
  */

  if (creal(r[0]) < creal(r[1]))
    {
      s[0] = creal(r[1]);
      s[1] = creal(r[0]);
    }
  else
    {
      s[0] = creal(r[0]);
      s[1] = creal(r[1]);
    }

  if (!(s[1] > 0))
    {
      log_error("non-positive eigenvalue (%f) in metric tensor", s[1]);
      return 1;
    }

  E->major = sqrt(s[0]);
  E->minor = sqrt(s[1]);

  /*
    Since the si are the eigenvalues, the eigenvectors are
    in the direction of the ellipse's major and minor axes.
    A short calculation shows that the matrix R is

       [ cos^2(t)  sin(t) cos(t) ]
       [ sin(t) cos(t)  sin^2(t) ]

    so that d/c and b/a are tan(t) -- we chose so as to
    avoid nans
  */

  mat_t R = msmul(1 / (s[0] - s[1]), mres(M, s[1]));
  double t = atan((R.a < R.d) ? (R.d / R.c) : (R.b / R.a));

  if (t < 0) t += M_PI;

  E->theta = t;

  return 0;
}

int ellipse_mat(const ellipse_t *e, mat_t *M)
{
  double
    a = e->major,
    b = e->minor;
  mat_t
    A = {a * a, 0, 0, b * b},
    R = mrot(e->theta),
    S = mtranspose(R);

  *M = mmmul(R, mmmul(A, S));

  return 0;
}

int ellipse_tangent_points(const ellipse_t *e, double t, vec_t *v)
{
  /*
    first find the tangent points of an ellipse centered at the
    origin with major axis along the x-axis
  */

  double st, ct;

  sincos(t - e->theta, &st, &ct);

  double
    a = e->major,
    b = e->minor,
    a2 = a * a,
    b2 = b * b,
    D = hypot(a * st, b * ct);

  vec_t u[2] =
    {
     {a2 * st / D, -b2 * ct / D},
     {-a2 * st / D, b2 * ct / D}
    };

  /*
    now rotate those points to the orientation of the ellipse
    and translate it by the position vector of the ellipse's
    centre
  */

  mat_t R = mrot(e->theta);

  for (size_t i = 0 ; i < 2 ; i++)
    v[i] = vadd(e->centre, mvmul(R, u[i]));

  return 0;
}

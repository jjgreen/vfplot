#ifndef GEOM2D_CONTACT_H
#define GEOM2D_CONTACT_H

#include <geom2d/ellipse.h>
#include <geom2d/vec.h>
#include <geom2d/mat.h>

int contact(const ellipse_t*, const ellipse_t*, double*, vec_t*);
int contact_mt(vec_t, mat_t, mat_t, double*, vec_t*);
int contact_mt0(vec_t, mat_t, mat_t, double, double*, vec_t*);

#endif

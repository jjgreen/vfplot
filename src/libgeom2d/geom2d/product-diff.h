/*
  product-diff.h
  Copyright (c) J.J. Green 2023
*/

#ifndef GEOM2D_PRODUCT_DIFF_H
#define GEOM2D_PRODUCT_DIFF_H

double product_diff(double, double, double, double);

#endif

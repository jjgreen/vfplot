#ifndef GEOM2D_ELLIPSE_H
#define GEOM2D_ELLIPSE_H

#include <geom2d/vec.h>
#include <geom2d/mat.h>

#include <stdbool.h>

typedef struct
{
  double major, minor, theta;
  vec_t centre;
} ellipse_t;

int mat_ellipse(const mat_t*, ellipse_t*);
int ellipse_mat(const ellipse_t*, mat_t*);
int ellipse_tangent_points(const ellipse_t*, double, vec_t*);

#endif

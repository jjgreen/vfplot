#ifndef GEOM2D_QUADRATIC_H
#define GEOM2D_QUADRATIC_H

#include <complex.h>

int quadratic_roots(const double[static 3], double[static 2]);
int quadratic_roots_complex(const double[static 3], double complex[static 2]);

#endif

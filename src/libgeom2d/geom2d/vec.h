#ifndef GEOM2D_VEC_H
#define GEOM2D_VEC_H

#include <stdbool.h>

typedef struct { double x, y; } vec_t;
typedef enum { rightward, leftward } bend_t;

vec_t vsub(vec_t, vec_t);
vec_t vadd(vec_t, vec_t);
vec_t vscale(double, vec_t);
double vabs(vec_t);
double vabs2(vec_t);
double vang(vec_t);
double vdot(vec_t, vec_t);
double vdet(vec_t, vec_t);
double vxtang(vec_t, vec_t);
vec_t vunit(vec_t);
bool visect(vec_t, vec_t, vec_t, vec_t);
bend_t bend3p(vec_t, vec_t, vec_t);
bend_t bend2v(vec_t, vec_t);

#endif

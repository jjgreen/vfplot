#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include "geom2d/contact.h"
#include "geom2d/sdcontact.h"

#ifndef SDCONTACT_SMALL
#define SDCONTACT_SMALL 0.05
#endif

/*
  The maximising x, f(x) for the quadratic f(x) = ax^2 + bx + c
  which satisfies f(0) = f0, f'(0) = df0, and f''(0) = ddf0.
*/

static int taylor_max(double f0, double df0, double ddf0,
                      double *xmax, double *fmax)
{
  double
    a = ddf0 / 2,
    b = df0,
    c = f0,
    x = -b / (2 * a);

  if ((x < 0) || (x > 1))
    return 1;

  *xmax = x;
  *fmax = (a * x + b) * x + c;

  return 0;
}

/*
  The maximising x for the quadratic f(x) = ax^2 + bx + c
  which satisfies f(0) = f0, f'(0) = df0, and f(1) = 0.
*/

static int sdquad_max(double f0, double df0, double *xmax)
{
  double b = df0, c = f0, a = -(b + c);

  *xmax = a ? (-b / (2 * a)) : 0.5;

  return 0;
}

int sdcontact_mt(vec_t rAB, mat_t A, mat_t B, double *fmax, vec_t *vmax)
{
  mat_t
    aA = madj(A),
    aB = madj(B),
    aAB = mmmul(aA, B),
    aBA = mmmul(aB, A);
  double
    D = mtrace(aAB),
    D2 = D * D,
    f0 = mqform(aA, rAB) / D;
  mat_t
    aBAaB = mmmul(aBA, aB),
    aABaA = mmmul(aAB, aA);
  double
    df0 = mqform(msub(aBAaB, aABaA), rAB) / D2;

  /*
    If the derivative of f at zero is not positive then f is
    maximised at zero
  */

  if (df0 <= 0.0)
    {
      *fmax = f0;
      if (vmax != NULL)
        *vmax = mpdir(aA, rAB);
      return 0;
    }

  /*
    Derivative of f at zero is positive, so f is maximised in
    the interior of [0, 1].  If that is very close to zero them
    we may run into floating-point problems calculating the
    value with contact(), but then the Taylor approximation to
    f at zero should be accurate.  Of course, we need at least
    the quadratic term of Taylor to get an approximation with
    a maximum, that needs the second derivative of f at zero.
  */

  double
    D3 = D2 * D,
    ddf0 = -2 * mdet(B) * mqform(aBAaB, rAB) / D3,
    xm, fm;

  if (taylor_max(f0, df0, ddf0, &xm, &fm) == 0)
    {
      if (xm < SDCONTACT_SMALL)
        {
          *fmax = fm;
          if (vmax != NULL)
            *vmax = mpdir(aA, rAB);
          return 0;
        }
    }

  /*
    So if the Taylor approximation gives a maxima close to zero then
    we take it, but otherwise the location of the maximum from Taylor
    should not be expected to be a good one to start off the regular
    contact iteration (it may not even be in [0, 1]).  Instead we use
    the quadratic which agrees with f(0), f'(0) and f(1) = 0, find its
    maximising x and use that as our starting point.
  */

  if (sdquad_max(f0, df0, &xm) != 0)
    return 1;

  return contact_mt0(rAB, A, B, xm, fmax, vmax);
}

int sdcontact(const ellipse_t *A, const ellipse_t *B, double *fmax, vec_t *vmax)
{
  mat_t MA, MB;

  if ((ellipse_mat(A, &MA) != 0) || (ellipse_mat(B, &MB) != 0))
    return 1;

  vec_t rAB = vsub(B->centre, A->centre);

  return sdcontact_mt(rAB, MA, MB, fmax, vmax);
}

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef HAVE_SINCOS

#include <math.h>

void sincos(double t, double *s, double *c)
{
  *s = sin(t);
  *c = cos(t);
}

#else

typedef void iso_forbids_an_empty_translation_unit;

#endif

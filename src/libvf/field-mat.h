#ifndef FIELD_MAT_H
#define FIELD_MAT_H

#include "vf/field.h"

field_t* field_read_mat(const char*);

#endif

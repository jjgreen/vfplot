#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "vf/circular.h"

#define A4SCALE 0.002

static int vector(void *arg, double x, double y, double *t, double *m)
{
  circular_field_t *field = arg;
  double M = field->scale;

  *t = atan2(y, x) - M_PI / 2;
  *m = M * A4SCALE;

  return 0;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static int curvature(void *arg, double x, double y, double *curv)
{
  double r;

  if ((r = hypot(x, y)) <= 0.0)
    return 1;

  *curv = 1 / r;

  return 0;
}

#pragma GCC diagnostic pop

static domain_t* domain(const void *arg)
{
  const circular_field_t *field = arg;
  domain_t *dom;

  if ((dom = domain_new()) != NULL)
    {
      double w = field->width, h = field->height;
      bbox_t b = {{-w / 2, w / 2}, {-h / 2, h / 2}};
      vec_t v = {0, 0};
      double R = w / 10;
      int err = 0;

      {
        polygon_t p;

        if (polygon_rect(b, &p) != 0)
          err++;
        else
          {
            if (domain_insert(dom, &p) != 0)
              err++;
            polygon_clear(&p);
          }
      }

      {
        polygon_t p;

        if (polygon_ngon(R, v, 32, &p) != 0)
          err++;
        else
          {
            if (domain_insert(dom, &p) != 0)
              err++;
            polygon_clear(&p);
          }
      }

      if (err == 0)
        {
          if (domain_orientate(dom) == 0)
            return dom;
        }

      domain_destroy(dom);
    }

  return NULL;
}

static void scale(void *arg, double M)
{
  circular_field_t *field = arg;
  field->scale = M;
}

vf_methods_t circular_methods =
  {
   .vector = vector,
   .curvature = curvature,
   .domain = domain,
   .scale = scale
  };

#ifndef FIELD_RANGE_BBOX_H
#define FIELD_RANGE_BBOX_H

#include "vf/field-align.h"
#include "vf/field-range.h"

#include <plot/bbox.h>

#include <stdlib.h>

int field_range_bbox(const field_range_t*,
                     field_align_t,
                     size_t, size_t,
                     bbox_t*);

#endif

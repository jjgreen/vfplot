#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdlib.h>

#include "vf/smoska.h"

#define A4SCALE 3e-3

/*
  A field used as an example in the paper "Vector Field Interpolation
  with Radial Basis Function", by V Smolik and V. Skala,
  https://ep.liu.se/ecp/127/003/ecp16127003.pdf
*/

static int vector(void *arg, double x, double y, double *t, double *m)
{
  smoska_field_t *field = arg;
  double
    u = x * (x * x + 1) / 2 + y * (-x + (y / 2 - 1) * y + 0.5),
    v = x * x * y / 2 + x * (-y * y / 2 + y - 0.5) + y / 2 - 1;

  *t = atan2(v, u);
  *m = A4SCALE * hypot(u, v) * field->scale;

  return 0;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

static domain_t* domain(const void *arg)
{
  domain_t *dom;

  if ((dom = domain_new()) != NULL)
    {
      bbox_t b = {{-2, 2}, {-1, 3}};
      polygon_t p;

      if (polygon_rect(b, &p) == 0)
        {
          int err = domain_insert(dom, &p);
          polygon_clear(&p);
          if (err == 0) return dom;
        }
      domain_destroy(dom);
    }

  return NULL;
}

#pragma GCC diagnostic pop

static void scale(void *v, double M)
{
  smoska_field_t *field = v;
  field->scale = M;
}

vf_methods_t smoska_methods =
  {
   .vector = vector,
   .curvature = NULL,
   .domain = domain,
   .scale = scale
  };

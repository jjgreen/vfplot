#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <log.h>

#include "vf/field.h"
#include "vf/field-range.h"

#include "field-mat.h"
#include "field-type.h"
#include "field-range-bbox.h"

/* read matlab mat file with libmatio */

#ifdef WITH_MATLAB

#include <matio.h>

field_t* field_read_mat(const char *file)
{
  /* read matrices into dat*, an possibly ranges into rng* */

  mat_t *mat;

  if ((mat = Mat_Open(file, MAT_ACC_RDONLY)) == NULL)
    {
      log_error("failed read of %s", file);
      return NULL;
    }

  char
    *datn[2] = {"u", "v"},
    *rngn[2] = {"xrange", "yrange"};
  matvar_t
    *datv[2],
    *rngv[2],
    *pixv;

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if ((datv[i] = Mat_VarRead(mat, datn[i])) == NULL)
	{
	  log_error("failed read of matrix %s from %s", datn[i], file);
	  return NULL;
	}
    }

  for (size_t i = 0 ; i < 2 ; i++)
    rngv[i] = Mat_VarRead(mat, rngn[i]);

  pixv = Mat_VarRead(mat, "pixel");

  Mat_Close(mat);

  /* check data is feasible */

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (rngv[i] == NULL)
        continue;

      if (rngv[i]->isComplex)
	{
	  log_error("%s is complex, expecting real", rngn[i]);
	  return NULL;
	}

      int rank;

      if ((rank = rngv[i]->rank) != 2)
	{
	  log_error("%s is not a matrix (rank %i)", rngn[i], rank);
	  return NULL;
	}

      size_t *n = rngv[i]->dims;

      if (n[0] * n[1] != 2)
	{
 	  log_error("%s not two-element (%zix%zi)", rngn[i], n[0], n[1]);
	  return NULL;
	}
    }

  if (pixv != NULL)
    {
      if (! pixv->isLogical)
        {
          log_error("pixel is not logical");
          return NULL;
        }

      int rank;

      if ((rank = pixv->rank) != 2)
	{
	  log_error("pixel is not a matrix (rank %i)", rank);
	  return NULL;
	}

      size_t *n = pixv->dims;

      if (n[0] * n[1] != 1)
	{
 	  log_error("pixel not 1-element (%zix%zi)", n[0], n[1]);
	  return NULL;
	}
    }

  for (size_t i = 0 ; i < 2 ; i++)
    {
      int rank;

      if ((rank = datv[i]->rank) != 2)
	{
	  log_error("%s is not a matrix (rank %i)", datn[i], rank);
	  return NULL;
	}
    }

  /* get the data matrices' order and check that they are the same */

  size_t dn[2];

  for (size_t i = 0 ; i < 2 ; i++)
    {
      if (datv[0]->dims[i] != datv[1]->dims[i])
	{
	  log_error("data matrices have different orders:");
	  for (size_t j = 0 ; j < 2 ; j++)
	    log_error("%s is %zi x %zi",
		    datn[j],
		    datv[j]->dims[0],
		    datv[j]->dims[1]);
	  return NULL;
	}
      dn[i] = datv[0]->dims[i];
    }

  /* read range into field range (if present) */

  bool have_range;
  field_range_t range;

  if ((rngv[0] != NULL) && (rngv[1] != NULL))
    {
      int
        ct0 = rngv[0]->class_type,
        ct1 = rngv[1]->class_type;

      if (ct0 != ct1)
        {
          log_error("ranges with differing classes");
          return NULL;
        }

      int ct = ct0;

      switch (ct)
        {
        case MAT_C_DOUBLE:
          range.x.min = ((double*)(rngv[0]->data))[0];
          range.x.max = ((double*)(rngv[0]->data))[1];
          range.y.min = ((double*)(rngv[1]->data))[0];
          range.y.max = ((double*)(rngv[1]->data))[1];
          break;
        default:
          log_error("class type %i not handled yet", ct);
          return NULL;
        }

      have_range = true;
    }
  else
    have_range = false;

  for (size_t i = 0 ; i < 2 ; i++)
    if (rngv[i] != NULL)
      Mat_VarFree(rngv[i]);

  /* read pixel value */

  field_align_t align = align_node;

  if (pixv != NULL)
    {
      int value = *((uint8_t*)(pixv->data));
      if (value)
        align = align_pixel;
    }

  if (pixv != NULL)
    Mat_VarFree(pixv);

  /* get bbox */

  size_t
    nx = dn[0],
    ny = dn[1];
  bbox_t bbox;

  if (have_range)
    {
      if (field_range_bbox(&range, align, nx, ny, &bbox) != 0)
        {
          log_error("failed to get bounding-box of range");
          return NULL;
        }
    }
  else
    {
      if (field_range_bbox(NULL, align, nx, ny, &bbox) != 0)
        {
          log_error("failed to get bounding-box absent range");
          return NULL;
        }
    }

  bilinear_t *B;

  if ((B = bilinear_new(nx, ny, 2)) == NULL)
    return NULL;

  bilinear_bbox_set(B, bbox);

  /* load data */

  for (size_t i = 0 ; i < nx ; i++)
    {
      for (size_t j = 0 ; j < ny ; j++)
        {
          size_t idx = nx * j + i;
          double z[2];

          for (size_t k = 0 ; k < 2 ; k++)
            {
              void *data = datv[k]->data;
              int ct = datv[k]->class_type;

              switch (ct)
                {
                case MAT_C_DOUBLE:
                  z[k] = ((double*)data)[idx];
		  break;
		case MAT_C_SINGLE:
		  z[k] = ((float*)data)[idx];
		  break;
		case MAT_C_INT32:
		  z[k] = ((mat_int32_t*)data)[idx];
		  break;
		case MAT_C_UINT32:
		  z[k] = ((mat_uint32_t*)data)[idx];
		  break;
		case MAT_C_INT16:
		  z[k] = ((mat_int16_t*)data)[idx];
		  break;
		case MAT_C_UINT16:
		  z[k] = ((mat_uint16_t*)data)[idx];
		  break;
		case MAT_C_INT8:
		  z[k] = ((mat_int8_t*)data)[idx];
		  break;
		case MAT_C_UINT8:
		  z[k] = ((mat_uint8_t*)data)[idx];
		  break;
		default:
		  log_error("%s class type %i not handled yet", datn[k], ct);
                  goto error_exit;
		}
	    }
          bilinear_z_set(B, i, j, z);
	}
    }

  for (size_t i = 0 ; i < 2 ; i++)
    Mat_VarFree(datv[i]);

  /* output */

  field_t *F;

  if ((F = malloc(sizeof(field_t))) == NULL)
    goto error_exit;

  F->align = align;
  F->v = B;
  F->k = NULL;

  return F;

 error_exit:

  bilinear_destroy(B);
  return NULL;
}

#else

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"

field_t* field_read_mat(const char *file)
{
  log_error("compiled without Matlab file support");
  return NULL;
}

#pragma GCC diagnostic pop

#endif

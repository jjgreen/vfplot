#ifndef VF_FIELD_FORMAT_H
#define VF_FIELD_FORMAT_H

typedef enum {
  format_none,
  format_csv,
  format_grd2,
  format_gfs,
  format_mat,
  format_unknown
} field_format_t;

#endif

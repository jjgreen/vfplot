#ifndef VF_FIELD_H
#define VF_FIELD_H

#include <vf/type.h>
#include <vf/field-align.h>
#include <vf/field-format.h>
#include <vf/field-grid.h>
#include <vf/field-range.h>

#include <plot/bbox.h>

#define INPUT_FILES_MAX 2

typedef struct field_t field_t;

field_t* field_read(field_format_t,
                    field_align_t,
                    int, const char**,
                    const field_grid_t*,
                    const field_range_t*);
void field_destroy(field_t*);

field_align_t field_align(const field_t*);
int field_scale(field_t*, double);

extern vf_methods_t field_methods;

#endif

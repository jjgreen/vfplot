/*
  Copyright (c) 2020 rxi
  Modified by J.J. Green for Seaview Sensing Ltd., those modification
  copyright Seaview Senisng Ltd., released under the same licence.

  Permission is hereby granted, free of charge, to any person obtaining a copy
  of this software and associated documentation files (the "Software"), to
  deal in the Software without restriction, including without limitation the
  rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
  sell copies of the Software, and to permit persons to whom the Software is
  furnished to do so, subject to the following conditions:

  The above copyright notice and this permission notice shall be included in
  all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
  IN THE SOFTWARE.
*/

#include "log.h"
#include <string.h>

#define MAX_CALLBACKS 8

typedef void (log_write_t)(log_event_t*);

typedef struct
{
  log_write_t *fn;
  void *udata;
  int level;
} callback_t;

static struct
{
  void *udata;
  log_lock_t *lock;
  int level;
  callback_t callbacks[MAX_CALLBACKS];
} L;

static const char *level_strings[] = {
  "TRACE",
  "DEBUG",
  "INFO",
  "WARN",
  "ERROR",
  "FATAL"
};

static const size_t level_count = sizeof(level_strings) / sizeof(char*);

static void callback_decorated(log_event_t *ev)
{
  char buf[64];
  buf[strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S", ev->time)] = '\0';
  fprintf(ev->udata, "%s %-5s %s:%d: ", buf,
          level_strings[ev->level], ev->file, ev->line);
  vfprintf(ev->udata, ev->fmt, ev->ap);
  fprintf(ev->udata, "\n");
  fflush(ev->udata);
}


static void callback_plain(log_event_t *ev)
{
  vfprintf(ev->udata, ev->fmt, ev->ap);
  fprintf(ev->udata, "\n");
  fflush(ev->udata);
}


static void lock(void)
{
  if (L.lock)
    L.lock(true, L.udata);
}


static void unlock(void)
{
  if (L.lock)
    L.lock(false, L.udata);
}


const char* log_level_string(int level)
{
  return level_strings[level];
}

int log_level_of_string(const char *string)
{
  for (size_t i = 0 ; i < level_count ; i++)
    {
      if (strcasecmp(string, level_strings[i]) == 0)
        return i;
    }
  return -1;
}

void log_set_lock(log_lock_t *fn, void *udata) {
  L.lock = fn;
  L.udata = udata;
}


void log_set_level(int level)
{
  L.level = level;
}


static int log_add_callback(log_write_t *fn, void *udata, int level)
{
  callback_t callback = { fn, udata, level };

  for (size_t i = 0; i < MAX_CALLBACKS; i++)
    {
      if (!L.callbacks[i].fn)
        {
          L.callbacks[i] = callback;
          return 0;
        }
    }
  return -1;
}


int log_add_decorated(FILE *fp, int level)
{
  return log_add_callback(callback_decorated, fp, level);
}


int log_add_plain(FILE *fp, int level)
{
  return log_add_callback(callback_plain, fp, level);
}

void log_reset(void)
{
  for (size_t i = 0 ; i < MAX_CALLBACKS ; i++)
    {
      callback_t *cb = &L.callbacks[i];

      cb->fn = NULL;
      cb->udata = NULL;
    }
}


static void init_event(log_event_t *ev, void *udata)
{
  if (!ev->time)
    {
      time_t t = time(NULL);
      ev->time = localtime(&t);
    }
  ev->udata = udata;
}


void log_log(int level, const char *file, int line, const char *fmt, ...)
{
  log_event_t ev =
    { .fmt = fmt, .file = file, .line  = line, .level = level };

  lock();

  for (size_t i = 0 ; i < MAX_CALLBACKS && L.callbacks[i].fn ; i++)
    {
      callback_t *cb = &L.callbacks[i];
      if (level >= cb->level)
        {
          init_event(&ev, cb->udata);
          va_start(ev.ap, fmt);
          cb->fn(&ev);
          va_end(ev.ap);
        }
    }

  unlock();
}

/*
  private/branches.h
  Copyright (c) J.J. Green 2020
*/

#ifndef PRIVATE_BRANCHES_H
#define PRIVATE_BRANCHES_H

#include <private/state.h>
#include <private/branch.h>

#include <rtree/error.h>

#include <stddef.h>
#include <errno.h>
#include <string.h>


inline branch_t* branches_get(const state_t *state, void *buffer, size_t i)
{
  const size_t branch_size = state_branch_size(state);
  char *bytes = (char*)buffer;
  return (branch_t*)(bytes + i * branch_size);
}

inline void branches_set(const state_t *state, void *buffer, size_t i,
                         const branch_t *src)
{
  memcpy(branches_get(state, buffer, i), src, state_branch_size(state));
}

#endif

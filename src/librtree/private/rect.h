/*
  private/rect.h
  Copyright (c) J.J. Green 2019
*/

#ifndef PRIVATE_RECT_H
#define PRIVATE_RECT_H

#include <private/state.h>
#include <rtree/types.h>

#include <stddef.h>
#include <stdbool.h>

int rect_init(const state_t*, rtree_coord_t*);

rtree_coord_t rect_volume(const state_t*, const rtree_coord_t*);
rtree_coord_t rect_spherical_volume(const state_t*, const rtree_coord_t*);
void rect_combine(const state_t*,
                  const rtree_coord_t *restrict,
                  const rtree_coord_t *restrict,
                  rtree_coord_t *restrict);
bool rect_intersect(const state_t*, const rtree_coord_t*, const rtree_coord_t*);
void rect_merge(const state_t*, const rtree_coord_t*, rtree_coord_t*);
void rect_copy(const state_t*, const rtree_coord_t*, rtree_coord_t*);
bool rect_identical(const state_t*, const rtree_coord_t*, const rtree_coord_t*);
int rects_alloc(const state_t*, size_t, rtree_coord_t**);
void rects_free(size_t, rtree_coord_t**);

#endif
